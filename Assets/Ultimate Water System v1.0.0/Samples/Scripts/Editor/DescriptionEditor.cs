﻿namespace UltimateWater.Utils
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(Description))]
    public class DescriptionEditor : Editor
    {
        #region Private Variables
        private GUIStyle _Style = new GUIStyle();

        private static Color _CommentColorPro = new Color(0.5f, 0.7f, 0.3f, 1.0f);
        private static Color _CommentColorFree = new Color(0.2f, 0.3f, 0.1f, 1.0f);

        private static Color _EditableColor = new Color(0.7f, 0.3f, 1.0f, 1.0f);
        #endregion Private Variables

        #region Unity Messages
        public sealed override void OnInspectorGUI()
        {
            var handle = target as Description;
            _Style.wordWrap = true;

            if (Description.Editable)
            {
                _Style.normal.textColor = _EditableColor;
                handle.Text = EditorGUILayout.TextArea(handle.Text, _Style);
            }
            else
            {
                _Style.normal.textColor = EditorGUIUtility.isProSkin ? _CommentColorPro : _CommentColorFree;
                EditorGUILayout.LabelField(handle.Text, _Style);
            }
        }
        #endregion Unity Messages
    }
}