﻿namespace UltimateWater
{
    using UnityEngine;
    using UnityEngine.Rendering;
    using Internal;

    public class DepthModule : IRenderModule
    {
        #region Public Methods

        public void OnEnable(WaterCamera waterCamera)
        {
            var depthFormat = Compatibility.GetFormat(RenderTextureFormat.Depth,
                new[] { RenderTextureFormat.RFloat, RenderTextureFormat.RHalf, RenderTextureFormat.R8 });

            var blendFormat = Compatibility.GetFormat(RenderTextureFormat.RFloat,
                new[] { RenderTextureFormat.RHalf });

            if (!depthFormat.HasValue || !blendFormat.HasValue) { return; }

            _Commands = new CommandBuffer { name = "[Water] : Render Depth" };

            _DepthTextureId = Shader.PropertyToID("_CameraDepthTexture2");
            _WaterlessDepthId = Shader.PropertyToID("_WaterlessDepthTexture");

            _DepthFormat = depthFormat.Value;
            _BlendedDepthFormat = depthFormat.Value;

            // only >= 4.0 shader targets can copy depth textures
            if (SystemInfo.graphicsShaderLevel < 50)
            {
                // get closest supported format for blended depth
                _BlendedDepthFormat = blendFormat.Value;

                // if supported format is precise (RFloat) but effect quality is low, decrease precision
                if (_BlendedDepthFormat == RenderTextureFormat.RFloat && waterCamera.BaseEffectsQuality < 0.2f)
                {
                    var lowBlend = Compatibility.GetFormat(RenderTextureFormat.RHalf);
                    if (!lowBlend.HasValue) { return; }

                    _BlendedDepthFormat = lowBlend.Value;
                }
            }
        }
        public void OnDisable(WaterCamera waterCamera)
        {
            Unbind(waterCamera);

            if (_DepthBlit != null)
            {
                _DepthBlit.Destroy();
                _DepthBlit = null;
            }

            if (_Commands != null)
            {
                _Commands.Release();
                _Commands = null;
            }
        }

        public void OnValidate(WaterCamera waterCamera)
        {
            ShaderUtility.Instance.Use(ShaderList.WaterDepth);
        }

        public void Process(WaterCamera waterCamera)
        {
            // check, if depth rendering is needed
            if (!waterCamera.RenderWaterDepth && (waterCamera.RenderMode != WaterRenderMode.ImageEffectDeferred))
            {
                return;
            }

            int waterDepthTextureId = ShaderUtility.Instance.PropertyId(ShaderVariableIDs.WaterDepthTexture);

            int width = Width(waterCamera);
            int height = Height(waterCamera);

            int depthBuffer = (_DepthFormat == RenderTextureFormat.Depth) ? 32 : 16;
            int waterlessPass = (_BlendedDepthFormat == RenderTextureFormat.Depth) ? 3 : 0;
            int depthPass = (_BlendedDepthFormat == RenderTextureFormat.Depth) ? 4 : 1;

            var effectFilter = waterCamera.BaseEffectsQuality > 0.98f ? FilterMode.Point : FilterMode.Bilinear;

            _Commands.Clear();
            _Commands.GetTemporaryRT(waterDepthTextureId, width, height, depthBuffer, effectFilter,
                _DepthFormat, RenderTextureReadWrite.Linear);

            _Commands.SetRenderTarget(waterDepthTextureId);
            _Commands.ClearRenderTarget(true, true, Color.white);

            waterCamera.AddWaterRenderCommands(_Commands, ShaderUtility.Instance.Get(ShaderList.WaterDepth), true, true,
                false);

            _Commands.GetTemporaryRT(_WaterlessDepthId, width, height, depthBuffer, FilterMode.Point,
                _BlendedDepthFormat,
                RenderTextureReadWrite.Linear);

            _Commands.SetRenderTarget(_WaterlessDepthId);
            _Commands.DrawMesh(UltimateWater.Internal.Quads.BipolarXInversedY, Matrix4x4.identity, DepthBlit, 0,
                waterlessPass);

            _Commands.GetTemporaryRT(_DepthTextureId, width, height, depthBuffer, FilterMode.Point,
                _BlendedDepthFormat,
                RenderTextureReadWrite.Linear);

            _Commands.SetRenderTarget(_DepthTextureId);
            _Commands.ClearRenderTarget(true, true, Color.white);
            _Commands.DrawMesh(UltimateWater.Internal.Quads.BipolarXInversedY, Matrix4x4.identity, DepthBlit, 0,
                depthPass);
            _Commands.SetGlobalTexture("_CameraDepthTexture", _DepthTextureId);

            Unbind(waterCamera);
            Bind(waterCamera);
        }
        public void Render(WaterCamera waterCamera, RenderTexture source, RenderTexture destination)
        {
        }
        #endregion Public Methods

        #region Private Variables
        private CommandBuffer _Commands;
        private CameraEvent _BoundEvent;
        private bool _IsBound;

        private RenderTextureFormat _DepthFormat;
        private RenderTextureFormat _BlendedDepthFormat;

        private Material DepthBlit
        {
            get
            {
                return _DepthBlit ??
                       (_DepthBlit = ShaderUtility.Instance.CreateMaterial(ShaderList.DepthCopy, HideFlags.DontSave));
            }
        }
        private Material _DepthBlit;

        private int _WaterlessDepthId;
        private int _DepthTextureId;
        #endregion Private Variables

        #region Private Methods
        private void Bind(WaterCamera waterCamera)
        {
            _IsBound = true;
            _BoundEvent = GetEvent(waterCamera);

            waterCamera.CameraComponent.AddCommandBuffer(_BoundEvent, _Commands);
        }
        private void Unbind(WaterCamera waterCamera)
        {
            if (!_IsBound)
            {
                return;
            }

            waterCamera.CameraComponent.RemoveCommandBuffer(_BoundEvent, _Commands);
            _IsBound = false;
        }

        private static int Width(WaterCamera waterCamera)
        {
            int result = waterCamera.BaseEffectWidth;
            if (waterCamera.RenderMode == WaterRenderMode.ImageEffectDeferred)
            {
                result >>= 1;
            }

            return result;
        }
        private static int Height(WaterCamera waterCamera)
        {
            int result = waterCamera.BaseEffectHeight;
            if (waterCamera.RenderMode == WaterRenderMode.ImageEffectDeferred)
            {
                result >>= 1;
            }

            return result;
        }

        private static CameraEvent GetEvent(WaterCamera waterCamera)
        {
            var camera = waterCamera.CameraComponent;

            bool isForwardRendering = camera.actualRenderingPath == RenderingPath.Forward;
            if (isForwardRendering)
            {
                bool isSinglePass = WaterProjectSettings.Instance.SinglePassStereoRendering;
                return isSinglePass ? CameraEvent.BeforeForwardOpaque : CameraEvent.AfterDepthTexture;
            }

            return CameraEvent.BeforeLighting;
        }
        #endregion Private Methods
    }
}