﻿namespace UltimateWater.Internal
{
    using UnityEngine;

    public abstract class IWaterModule
    {
        internal virtual void Start(Water water)
        {
        }

        internal abstract void Enable();
        internal abstract void Disable();

        internal abstract void Destroy();
        internal abstract void Validate();

        internal virtual void Update()
        {
        }

        internal virtual void OnWaterRender(WaterCamera camera)
        {
        }
        internal virtual void OnWaterPostRender(WaterCamera camera)
        {
        }
    }
}