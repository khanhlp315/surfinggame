﻿namespace UltimateWater
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Serialization;

    #region Public Types
    [System.Serializable]
    public struct WaterQualityLevel
    {
        [SerializeField]
        public string name;

        [SerializeField]
        [Tooltip("All simulations will be performed at most with this resolution")]
        public int maxSpectrumResolution;

        [SerializeField]
        public bool allowHighPrecisionTextures;

        [FormerlySerializedAs("allowHighQualitySlopeMaps")]
        [SerializeField]
        public bool allowHighQualityNormalMaps;

        [Range(0.0f, 1.0f)]
        [SerializeField]
        public float tileSizeScale;

        [SerializeField]
        public WaterWavesMode wavesMode;

        [SerializeField]
        public bool allowSpray;

        [Range(0.0f, 1.0f)]
        [SerializeField]
        public float foamQuality;

        [Range(0.0f, 1.0f)]
        [SerializeField]
        public float maxTesselationFactor;

        [SerializeField]
        public int maxVertexCount;

        [SerializeField]
        public int maxTesselatedVertexCount;

        [SerializeField]
        public bool allowAlphaBlending;

        public void ResetToDefaults()
        {
            name = "";
            maxSpectrumResolution = 256;
            allowHighPrecisionTextures = true;
            tileSizeScale = 1.0f;
            wavesMode = WaterWavesMode.AllowAll;
            allowSpray = true;
            foamQuality = 1.0f;
            maxTesselationFactor = 1.0f;
            maxVertexCount = 500000;
            maxTesselatedVertexCount = 120000;
            allowAlphaBlending = true;
        }
    }

    [System.Serializable]
    public class WaterRipplesData
    {
        #region Public Types
        [System.Serializable]
        public enum ShaderModes
        {
            ComputeShader,
            PixelShader
        }
        [System.Serializable]
        public class LodSettings
        {
            public float Distance;
            public int PixelsPerUnit;
        }
        #endregion Public Types

        #region Inspector Variables
        [Tooltip("Static objects that interact with water (terrain, pillars, rocks)")]
        [SerializeField]
        private LayerMask _StaticDepthMask;

        [Tooltip("How many simulation steps are performed per frame, the more iterations, the faster the water waves are, but it's expensive")]
        [SerializeField]
        private int _Iterations = 1;

        [Header("Texture formats")]
        [Tooltip("Simulation data (only R channel is used)")]
        [SerializeField]
        private RenderTextureFormat _SimulationFormat = RenderTextureFormat.RHalf;
        [Tooltip("Screen space simulation data gather (only R channel is used)")]
        [SerializeField]
        private RenderTextureFormat _GatherFormat = RenderTextureFormat.RGHalf;
        [Tooltip("Depth data (only R channel is used)")]
        [SerializeField]
        private RenderTextureFormat _DepthFormat = RenderTextureFormat.RHalf;

        [Header("Shaders")]
        [SerializeField]
        private ShaderModes _ShaderMode = ShaderModes.PixelShader;
        #endregion Inspector Variables

        #region Public Variables
        public int Iterations { get { return _Iterations; } }

        #region Shaders
        public ShaderModes ShaderMode
        {
            get { return _ShaderMode; }
        }
        #endregion Shaders

        #region Masks
        public LayerMask StaticDepthMask
        {
            get
            {
                return _StaticDepthMask;
            }
        }
        #endregion Masks

        #region Texture Formats
        public RenderTextureFormat SimulationFormat
        {
            get
            {
                return _SimulationFormat;
            }
        }
        public RenderTextureFormat GatherFormat
        {
            get
            {
                return _GatherFormat;
            }
        }
        public RenderTextureFormat DepthFormat
        {
            get
            {
                return _DepthFormat;
            }
        }
        #endregion Texture Formats
        #endregion Public Variables
    }

    public enum WaterWavesMode
    {
        AllowAll,
        AllowNormalFFT,
        AllowGerstner,
        DisallowAll
    }
    #endregion Public Types

    public class WaterQualitySettings : ScriptableObjectSingleton
    {
        #region Inspector Variables
        [SerializeField]
        private WaterQualityLevel[] qualityLevels;

        [SerializeField]
        private bool synchronizeWithUnity = true;

        [SerializeField]
        private int savedCustomQualityLevel;
        #endregion Inspector Variables

        #region Public Variables
        public WaterRipplesData Ripples;

        public event System.Action Changed;

        public string[] Names
        {
            get
            {
                string[] names = new string[qualityLevels.Length];

                for (int i = 0; i < qualityLevels.Length; ++i)
                    names[i] = qualityLevels[i].name;

                return names;
            }
        }

        public WaterWavesMode WavesMode
        {
            get { return currentQualityLevel.wavesMode; }
            set
            {
                if (currentQualityLevel.wavesMode == value)
                    return;

                currentQualityLevel.wavesMode = value;
                OnChange();
            }
        }

        public int MaxSpectrumResolution
        {
            get { return currentQualityLevel.maxSpectrumResolution; }
            set
            {
                if (currentQualityLevel.maxSpectrumResolution == value)
                    return;

                currentQualityLevel.maxSpectrumResolution = value;
                OnChange();
            }
        }
        public float TileSizeScale
        {
            get { return currentQualityLevel.tileSizeScale; }
            set
            {
                if (currentQualityLevel.tileSizeScale == value)
                    return;

                currentQualityLevel.tileSizeScale = value;
                OnChange();
            }
        }

        public bool AllowHighPrecisionTextures
        {
            get { return currentQualityLevel.allowHighPrecisionTextures; }
            set
            {
                if (currentQualityLevel.allowHighPrecisionTextures == value)
                    return;

                currentQualityLevel.allowHighPrecisionTextures = value;
                OnChange();
            }
        }
        public bool AllowHighQualityNormalMaps
        {
            get { return currentQualityLevel.allowHighQualityNormalMaps; }
            set
            {
                if (currentQualityLevel.allowHighQualityNormalMaps == value)
                    return;

                currentQualityLevel.allowHighQualityNormalMaps = value;
                OnChange();
            }
        }

        public float MaxTesselationFactor
        {
            get { return currentQualityLevel.maxTesselationFactor; }
            set
            {
                if (currentQualityLevel.maxTesselationFactor == value)
                    return;

                currentQualityLevel.maxTesselationFactor = value;
                OnChange();
            }
        }
        public int MaxVertexCount
        {
            get { return currentQualityLevel.maxVertexCount; }
            set
            {
                if (currentQualityLevel.maxVertexCount == value)
                    return;

                currentQualityLevel.maxVertexCount = value;
                OnChange();
            }
        }
        public int MaxTesselatedVertexCount
        {
            get { return currentQualityLevel.maxTesselatedVertexCount; }
            set
            {
                if (currentQualityLevel.maxTesselatedVertexCount == value)
                    return;

                currentQualityLevel.maxTesselatedVertexCount = value;
                OnChange();
            }
        }
        public bool AllowAlphaBlending
        {
            get { return currentQualityLevel.allowAlphaBlending; }
            set
            {
                if (currentQualityLevel.allowAlphaBlending == value)
                    return;

                currentQualityLevel.allowAlphaBlending = value;
                OnChange();
            }
        }

        public static WaterQualitySettings Instance
        {
            get
            {
                // ReSharper disable once RedundantCast.0
                if ((object)instance == null)
                {
                    instance = LoadSingleton<WaterQualitySettings>();
                    instance.Changed = null;
                    instance.waterQualityIndex = -1;
                    instance.SynchronizeQualityLevel();
                }

                return instance;
            }
        }

        /// <summary>
        /// Are water quality settings synchronized with Unity?
        /// </summary>
        public bool SynchronizeWithUnity
        {
            get { return synchronizeWithUnity; }
        }
        #endregion Public Variables

        #region Public Methods
        public int GetQualityLevel()
        {
            return waterQualityIndex;
        }

        public void SetQualityLevel(int index)
        {
            if (!Application.isPlaying)
                savedCustomQualityLevel = index;

            currentQualityLevel = qualityLevels[index];
            waterQualityIndex = index;

            OnChange();
        }

        /// <summary>
        /// Synchronizes current water quality level with the one set in Unity quality settings.
        /// </summary>
        public void SynchronizeQualityLevel()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying && synchronizeWithUnity)
                SynchronizeLevelNames();
#endif

            int currentQualityIndex = -1;

            if (synchronizeWithUnity)
                currentQualityIndex = FindQualityLevel(QualitySettings.names[QualitySettings.GetQualityLevel()]);

            if (currentQualityIndex == -1)
                currentQualityIndex = savedCustomQualityLevel;

            currentQualityIndex = Mathf.Clamp(currentQualityIndex, 0, qualityLevels.Length - 1);

            if (currentQualityIndex != waterQualityIndex)
                SetQualityLevel(currentQualityIndex);
        }
        #endregion Public Methods

        #region Private Variables
        private int waterQualityIndex;
        // working copy of the current quality level free for temporary modifications
        private WaterQualityLevel currentQualityLevel;
        private static WaterQualitySettings instance;
        #endregion Private Variables

        #region Private Methods
        internal WaterQualityLevel[] GetQualityLevelsDirect()
        {
            return qualityLevels;
        }

        internal WaterQualityLevel CurrentQualityLevel
        {
            get { return currentQualityLevel; }
        }

        private void OnChange()
        {
            if (Changed != null)
                Changed();
        }

        private int FindQualityLevel(string name)
        {
            for (int i = 0; i < qualityLevels.Length; ++i)
            {
                if (qualityLevels[i].name == name)
                    return i;
            }

            return -1;
        }

        /// <summary>
        /// Ensures that water quality settings are named the same way that Unity ones.
        /// </summary>
        private void SynchronizeLevelNames()
        {
#if UNITY_EDITOR
            if (qualityLevels == null)
                qualityLevels = new WaterQualityLevel[0];

            string[] unityNames = QualitySettings.names;
            var currentNames = Names;
            int index = 0;
            bool noChanges = true;

            foreach (var name in currentNames)
            {
                if (unityNames[index] != name)
                {
                    noChanges = false;
                    break;
                }
            }

            if (noChanges)
                return;

            var matches = new List<WaterQualityLevel>();
            var availableLevels = new List<WaterQualityLevel>(qualityLevels);
            var availableUnityLevels = new List<string>(unityNames);

            // keep existing levels with matching names
            for (int i = 0; i < availableLevels.Count; ++i)
            {
                var level = availableLevels[i];

                if (availableUnityLevels.Contains(level.name))
                {
                    matches.Add(level);
                    availableLevels.RemoveAt(i--);
                    availableUnityLevels.Remove(level.name);
                }
            }

            // use non-matched levels as-is // possibly just their name or order has changed
            while (availableLevels.Count > 0 && availableUnityLevels.Count > 0)
            {
                var level = availableLevels[0];
                level.name = availableUnityLevels[0];

                matches.Add(level);

                availableLevels.RemoveAt(0);
                availableUnityLevels.RemoveAt(0);
            }

            // create new levels if there is more of them left
            while (availableUnityLevels.Count > 0)
            {
                var level = new WaterQualityLevel();
                level.ResetToDefaults();
                level.name = availableUnityLevels[0];

                matches.Add(level);

                availableUnityLevels.RemoveAt(0);
            }

            // create new list with the same order as in Unity
            qualityLevels = new WaterQualityLevel[unityNames.Length];

            for (int i = 0; i < qualityLevels.Length; ++i)
                qualityLevels[i] = matches.Find(l => l.name == unityNames[i]);

            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }
        #endregion Private Methods
    }
}