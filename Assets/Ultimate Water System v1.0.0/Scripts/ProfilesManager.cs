﻿using System.Collections.Generic;
using UltimateWater.Internal;

namespace UltimateWater
{
    using System;
    using UnityEngine;

    [Serializable]
    public class ProfilesManager : IWaterModule
    {
        #region Inspector Variables
        [SerializeField]
        private WaterProfile initialProfile;

        [SerializeField]
        private Water.WaterEvent changed;
        #endregion Inspector Variables

        #region Public Variables
        /// <summary>
        /// Currently set water profiles with their associated weights.
        /// </summary>
        public Water.WeightedProfile[] Profiles { get; private set; }

        public Water.WaterEvent Changed
        {
            get { return changed; }
        }
        #endregion Public Variables

        #region Public Methods
        /// <summary>
        /// Caches profiles for later use to avoid hiccups.
        /// </summary>
        /// <param name="profiles"></param>
        public void CacheProfiles(params WaterProfileData[] profiles)
        {
            var windWaves = _Water.WindWaves;

            if (windWaves != null)
            {
                for (int i = 0; i < profiles.Length; ++i)
                {
                    windWaves.SpectrumResolver.CacheSpectrum(profiles[i].Spectrum);
                }
            }
        }

        /// <summary>
        /// Lets you quickly evaluate a choosen property from the water profiles by using a lambda expression.
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        public float EvaluateProfilesParameter(Func<WaterProfileData, float> func)
        {
            float sum = 0.0f;

            var profiles = Profiles;

            for (int i = profiles.Length - 1; i >= 0; --i)
            {
                sum += func(profiles[i].Profile) * profiles[i].Weight;
            }

            return sum;
        }

        /// <summary>
        /// Sets water profiles with custom weights.
        /// </summary>
        /// <param name="profiles"></param>
        public void SetProfiles(params Water.WeightedProfile[] profiles)
        {
            for (int i = 0; i < profiles.Length; ++i)
            {
                CacheProfiles(profiles[i].Profile);
            }

            CheckProfiles(profiles);

            Profiles = profiles;
            _ProfilesDirty = true;
        }

        /// <summary>
        /// Instantly populates all water properties from the used profiles. Normally it's delayed and done on each update.
        /// </summary>
        public void ValidateProfiles()
        {
            bool valuesDirty = false;
            foreach (var profile in Profiles)
            {
                valuesDirty |= profile.Profile.Dirty;
            }

            if (valuesDirty || _ProfilesDirty)
            {
                _ProfilesDirty = false;
                changed.Invoke(_Water);
            }
        }
        #endregion Public Methods

        #region Private Variables
        private Water _Water;
        private bool _ProfilesDirty;
        private WaterProfile initialProfileCopy;
        #endregion Private Variables

        #region Private Methods
        internal override void Start(Water water)
        {
            _Water = water;

            if (changed == null)
            {
                changed = new Water.WaterEvent();
            }

#if UNITY_EDITOR
            if (initialProfile == null)
                initialProfile = WaterPackageUtilities.FindDefaultAsset<WaterProfile>("\"Sea - 6. Strong Breeze\" t:WaterProfile", "t:WaterProfile");
#endif

            if (Profiles == null)
            {
                if (initialProfile != null)
                    SetProfiles(new Water.WeightedProfile(initialProfile, 1.0f));
                else
                    Profiles = new Water.WeightedProfile[0];
            }

            WaterQualitySettings.Instance.Changed -= OnQualitySettingsChanged;
            WaterQualitySettings.Instance.Changed += OnQualitySettingsChanged;
        }

        internal override void Enable()
        {
            _ProfilesDirty = true;
        }

        internal override void Disable()
        {
        }

        internal override void Destroy()
        {
            WaterQualitySettings.Instance.Changed -= OnQualitySettingsChanged;
        }

        internal override void Update()
        {
            ValidateProfiles();
        }

        internal override void Validate()
        {
            if (Profiles != null && Profiles.Length != 0 && (initialProfileCopy == initialProfile || initialProfileCopy == null))
            {
                initialProfileCopy = initialProfile;
                _ProfilesDirty = true;
            }
            else if (initialProfile != null)
            {
                initialProfileCopy = initialProfile;
                Profiles = new[] { new Water.WeightedProfile(initialProfile, 1.0f) };
                _ProfilesDirty = true;
            }
        }

        private void OnQualitySettingsChanged()
        {
            _ProfilesDirty = true;
        }

        /// <summary>
        /// Ensures that profiles are fine to use.
        /// </summary>
        /// <param name="profiles"></param>
        private static void CheckProfiles(IList<Water.WeightedProfile> profiles)
        {
            if (profiles == null)
            {
                return;
            }

            if (profiles.Count == 0)
                throw new ArgumentException("Water has to use at least one profile.");

            float tileSize = profiles[0].Profile.TileSize;

            for (int i = 1; i < profiles.Count; ++i)
            {
                if (profiles[i].Profile.TileSize != tileSize)
                {
                    Debug.LogError("TileSize varies between used water profiles. It is the only parameter that you should keep equal on all profiles used at a time.");
                    break;
                }
            }
        }
        #endregion Private Methods
    }
}