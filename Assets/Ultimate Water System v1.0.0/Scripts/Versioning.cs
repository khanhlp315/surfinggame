﻿namespace UltimateWater.Utils
{
    public class Versioning
    {
        #region Public Variables
        public static string Name { get { return "1.0.0"; } }
        public static int Number { get { return 100; } }
        #endregion Public Variables
    }
}