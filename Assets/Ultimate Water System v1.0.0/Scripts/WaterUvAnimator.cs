﻿using UnityEngine;

namespace UltimateWater.Internal
{
    /// <summary>
    ///     Animates water UV in time to simulate a water flow.
    /// </summary>
    [System.Serializable]
    public sealed class WaterUvAnimator : IWaterModule
    {
        #region Public Variables
        public Vector2 WindOffset
        {
            get { return new Vector2(windOffset1x, windOffset1y); }
        }

        public NormalMapAnimation NormalMapAnimation1
        {
            get { return _NormalMapAnimation1; }
            set
            {
                _NormalMapAnimation1 = value;
                windVectorsDirty = true;
                uvTransform1.x = _NormalMapAnimation1.Tiling.x;
                uvTransform1.y = _NormalMapAnimation1.Tiling.y;
            }
        }

        public NormalMapAnimation NormalMapAnimation2
        {
            get { return _NormalMapAnimation2; }
            set
            {
                _NormalMapAnimation2 = value;
                windVectorsDirty = true;
                uvTransform2.x = _NormalMapAnimation2.Tiling.x;
                uvTransform2.y = _NormalMapAnimation2.Tiling.y;
            }
        }
        #endregion Public Variables

        #region Private Variables
        private NormalMapAnimation _NormalMapAnimation1 = new NormalMapAnimation(1.0f, -10.0f, 1.0f, new Vector2(1.0f, 1.0f));
        private NormalMapAnimation _NormalMapAnimation2 = new NormalMapAnimation(-0.55f, 20.0f, 0.74f, new Vector2(1.5f, 1.5f));

        private float windOffset1x, windOffset1y;
        private float windOffset2x, windOffset2y;
        private Vector2 windSpeed1;
        private Vector2 windSpeed2;
        private Vector2 windSpeed;

        private Water water;
        private WindWaves windWaves;
        private bool hasWindWaves;

        private int bumpMapST;
        private int detailAlbedoMapST;
        private Vector4 uvTransform1;
        private Vector4 uvTransform2;
        private bool windVectorsDirty = true;

        private float lastTime;
        #endregion Private Variables

        #region Internal Methods
        internal override void Start(Water water)
        {
            this.water = water;
            this.windWaves = water.WindWaves;
            this.hasWindWaves = windWaves != null;

            bumpMapST = Shader.PropertyToID("_BumpMap_ST");
            detailAlbedoMapST = Shader.PropertyToID("_DetailAlbedoMap_ST");
        }

        internal override void Update()
        {
            float time = water.Time;
            float deltaTime = time - lastTime;
            lastTime = time;

            if (windVectorsDirty || HasWindSpeedChanged())
            {
                PrecomputeWindVectors();
                windVectorsDirty = false;
            }

            // apply offset
            windOffset1x += windSpeed1.x * deltaTime;
            windOffset1y += windSpeed1.y * deltaTime;
            windOffset2x += windSpeed2.x * deltaTime;
            windOffset2y += windSpeed2.y * deltaTime;

            uvTransform1.z = -windOffset1x * uvTransform1.x;
            uvTransform1.w = -windOffset1y * uvTransform1.y;

            uvTransform2.z = -windOffset2x * uvTransform2.x;
            uvTransform2.w = -windOffset2x * uvTransform2.y;

            // apply to material
            var block = water.Renderer.PropertyBlock;
            block.SetVector(bumpMapST, uvTransform1);
            block.SetVector(detailAlbedoMapST, uvTransform2);
        }
        #endregion Internal Methods

        #region Private Methods
        private void PrecomputeWindVectors()
        {
            windSpeed = GetWindSpeed();
            windSpeed1 = FastMath.Rotate(windSpeed, _NormalMapAnimation1.Deviation * Mathf.Deg2Rad) * (_NormalMapAnimation1.Speed * 0.001365f);
            windSpeed2 = FastMath.Rotate(windSpeed, _NormalMapAnimation2.Deviation * Mathf.Deg2Rad) * (_NormalMapAnimation2.Speed * 0.00084f);
        }

        private Vector2 GetWindSpeed()
        {
            return hasWindWaves ? windWaves.WindSpeed : new Vector2(1.0f, 0.0f);
        }

        private bool HasWindSpeedChanged()
        {
            return hasWindWaves && windWaves.WindSpeedChanged;
        }
        #endregion Private Methods

        #region Unused Inherited
        internal override void Destroy()
        {
        }

        internal override void Validate()
        {
        }

        internal override void Enable()
        {
        }

        internal override void Disable()
        {
        }
        #endregion Unused Inherited
    }
}