﻿namespace UltimateWater
{
    using UnityEngine;

    [System.Serializable]
    public class WaterProfileData
    {
        #region Public Types
        public enum WaterSpectrumType
        {
            Phillips,
            Unified
        }
        #endregion Public Types

        #region Inspector Variables
        [SerializeField]
        private WaterSpectrumType spectrumType = WaterSpectrumType.Unified;

        [SerializeField]
        private float windSpeed = 22.0f;

        [Tooltip("Tile size in world units of all water maps including heightmap. High values lower overall quality, but low values make the water pattern noticeable.")]
        [SerializeField]
        private float tileSize = 180.0f;

        [SerializeField]
        private float tileScale = 1.0f;

        [Tooltip("Setting it to something else than 1.0 will make the spectrum less physically correct, but still may be useful at times.")]
        [SerializeField]
        private float wavesAmplitude = 1.0f;

        [SerializeField]
        private float wavesFrequencyScale = 1.0f;

        [Range(0.0f, 4.0f)]
        [SerializeField]
        private float horizontalDisplacementScale = 1.0f;

        [SerializeField]
        private float phillipsCutoffFactor = 2000.0f;

        [SerializeField]
        private float gravity = -9.81f;

        [Tooltip("It is the length of water in meters over which a wind has blown. Usually a distance to the closest land in the direction opposite to the wind.")]
        [SerializeField]
        private float fetch = 100000.0f;

        [Tooltip("Eliminates waves moving against the wind.")]
        [Range(0.0f, 1.0f)]
        [SerializeField]
        private float directionality = 0.0f;

        [ColorUsage(false, true, 0.0f, 10.0f, 0.0f, 10.0f)]
        [SerializeField]
        private Color absorptionColor = new Color(0.35f, 0.04f, 0.001f, 1.0f);

        [SerializeField]
        private bool customUnderwaterAbsorptionColor = true;

        [Tooltip("Absorption color used by the underwater camera image-effect. Gradient describes color at each depth starting with 0 and ending on 600 units.")]
        [SerializeField]
        private Gradient absorptionColorByDepth;

        [SerializeField]
        private Gradient absorptionColorByDepthFlatGradient;

        [ColorUsage(false, true, 0.0f, 10.0f, 0.0f, 10.0f)]
        [SerializeField]
        private Color diffuseColor = new Color(0.1176f, 0.2196f, 0.2666f);

        [ColorUsage(false)]
        [SerializeField]
        private Color specularColor = new Color(0.0353f, 0.0471f, 0.0549f);

        [ColorUsage(false)]
        [SerializeField]
        private Color depthColor = new Color(0.0f, 0.0f, 0.0f);

        [ColorUsage(false)]
        [SerializeField]
        private Color emissionColor = new Color(0.0f, 0.0f, 0.0f);

        [ColorUsage(false)]
        [SerializeField]
        private Color reflectionColor = new Color(1.0f, 1.0f, 1.0f);

        [Range(0.0f, 1.0f)]
        [SerializeField]
        private float smoothness = 0.94f;

        [SerializeField]
        private bool customAmbientSmoothness = false;

        [Range(0.0f, 1.0f)]
        [SerializeField]
        private float ambientSmoothness = 0.94f;

        [Range(0.0f, 6.0f)]
        [SerializeField]
        private float isotropicScatteringIntensity = 1.0f;

        [Range(0.0f, 6.0f)]
        [SerializeField]
        private float forwardScatteringIntensity = 1.0f;

        [Range(0.0f, 1.0f)]
        [SerializeField]
        private float subsurfaceScatteringContrast = 0.0f;

        [ColorUsage(false, true, 1.0f, 8.0f, 1.0f, 8.0f)]
        [SerializeField]
        private Color subsurfaceScatteringShoreColor = new Color(1.4f, 3.0f, 3.0f);

        [Range(0.0f, 1.0f)]
        [SerializeField]
        private float refractionDistortion = 0.55f;

        [SerializeField]
        private float fresnelBias = 0.02040781f;

        [Range(0.5f, 20.0f)]
        [SerializeField]
        private float detailFadeDistance = 4.5f;

        [Range(0.1f, 10.0f)]
        [SerializeField]
        private float displacementNormalsIntensity = 2.0f;

        [Tooltip("Planar reflections are very good solution for calm weather, but you should fade them out for profiles with big waves (storms etc.) as they get completely incorrect then.")]
        [Range(0.0f, 1.0f)]
        [SerializeField]
        private float planarReflectionIntensity = 0.6f;

        [Range(1.0f, 10.0f)]
        [SerializeField]
        private float planarReflectionFlatten = 6.0f;

        [Tooltip("Fixes some artifacts produced by planar reflections at grazing angles.")]
        [Range(0.0f, 0.008f)]
        [SerializeField]
        private float planarReflectionVerticalOffset = 0.0015f;

        [SerializeField]
        private float edgeBlendFactor = 0.15f;

        [SerializeField]
        private float directionalWrapSSS = 0.2f;

        [SerializeField]
        private float pointWrapSSS = 0.5f;

        [Tooltip("Used by the physics.")]
        [SerializeField]
        private float density = 998.6f;

        [Range(0.0f, 0.03f)]
        [SerializeField]
        private float underwaterBlurSize = 0.003f;

        [Range(0.0f, 2.0f)]
        [SerializeField]
        private float underwaterLightFadeScale = 0.8f;

        [Range(0.0f, 0.4f)]
        [SerializeField]
        private float underwaterDistortionsIntensity = 0.05f;

        [Range(0.02f, 0.5f)]
        [SerializeField]
        private float underwaterDistortionAnimationSpeed = 0.1f;

        [Range(1.0f, 64.0f)]
        [SerializeField]
        private float dynamicSmoothnessIntensity = 1.0f;

        [SerializeField]
        private NormalMapAnimation normalMapAnimation1 = new NormalMapAnimation(1.0f, -10.0f, 1.0f, new Vector2(1.0f, 1.0f));

        [SerializeField]
        private NormalMapAnimation normalMapAnimation2 = new NormalMapAnimation(-0.55f, 20.0f, 0.74f, new Vector2(1.5f, 1.5f));

        [SerializeField]
        private Texture2D normalMap;

        [SerializeField]
        private float foamIntensity = 1.0f;

        [SerializeField]
        private float foamThreshold = 1.0f;

        [Tooltip("Determines how fast foam will fade out.")]
        [Range(0.0f, 1.0f)]
        [SerializeField]
        private float foamFadingFactor = 0.85f;

        [Range(0.0f, 5.0f)]
        [SerializeField]
        private float foamShoreIntensity = 1.0f;

        [Range(0.0f, 5.0f)]
        [SerializeField]
        private float foamShoreExtent = 1.0f;

        [SerializeField]
        private float foamNormalScale = 2.2f;

        [ColorUsage(false)]
        [SerializeField]
        private Color foamDiffuseColor = new Color(0.8f, 0.8f, 0.8f);

        [Tooltip("Alpha component is PBR smoothness.")]
        [SerializeField]
        private Color foamSpecularColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);

        [Range(0.0f, 4.0f)]
        [SerializeField]
        private float sprayThreshold = 1.0f;

        [Range(0.0f, 0.999f)]
        [SerializeField]
        private float spraySkipRatio = 0.9f;

        [Range(0.25f, 4.0f)]
        [SerializeField]
        private float spraySize = 1.0f;

        [SerializeField]
        private Texture2D foamDiffuseMap;

        [SerializeField]
        private Texture2D foamNormalMap;

        [SerializeField]
        private Vector2 foamTiling = new Vector2(5.4f, 5.4f);
        #endregion Inspector Variables

        #region Private Variables
        [SerializeField] [HideInInspector] private WaterProfile _Template;

        public WaterWavesSpectrum spectrum;
        public bool Dirty;
        #endregion Private Variables

        #region Public Variables
        public WaterProfileData Template
        {
            get { return _Template.Data; }
        }
        public WaterProfile TemplateProfile
        {
            set { _Template = value; }
        }
        public bool IsTemplate
        {
            get { return _Template == null; }
        }

        public WaterSpectrumType SpectrumType
        {
            get { return spectrumType; }
        }
        public WaterWavesSpectrum Spectrum
        {
            get
            {
                if (spectrum == null)
                {
                    CreateSpectrum();
                }

                return spectrum;
            }
        }

        public float WindSpeed
        {
            get { return windSpeed; }
        }
        public float TileSize
        {
            get { return tileSize; }
        }
        public float TileScale
        {
            get { return tileScale; }
        }
        public float HorizontalDisplacementScale
        {
            get { return horizontalDisplacementScale; }
            set
            {
                horizontalDisplacementScale = value;
                Dirty = true;
            }
        }

        public float Gravity
        {
            get { return gravity; }
            set
            {
                gravity = value;
                Dirty = true;
            }
        }
        public float Directionality
        {
            get { return directionality; }
            set
            {
                directionality = value;
                Dirty = true;
            }
        }

        public Color AbsorptionColor
        {
            get { return absorptionColor; }
            set
            {
                absorptionColor = value;
                Dirty = true;
            }
        }
        public Color DiffuseColor
        {
            get { return diffuseColor; }
            set
            {
                diffuseColor = value;
                Dirty = true;
            }
        }
        public Color SpecularColor
        {
            get { return specularColor; }
            set
            {
                specularColor = value;
                Dirty = true;
            }
        }
        public Color DepthColor
        {
            get { return depthColor; }
            set
            {
                depthColor = value;
                Dirty = true;
            }
        }
        public Color EmissionColor
        {
            get { return emissionColor; }
            set
            {
                emissionColor = value;
                Dirty = true;
            }
        }
        public Color ReflectionColor
        {
            get { return reflectionColor; }
            set
            {
                reflectionColor = value;
                Dirty = true;
            }
        }

        public float Smoothness
        {
            get { return smoothness; }
            set
            {
                smoothness = value;
                Dirty = true;
            }
        }
        public bool CustomAmbientSmoothness
        {
            get { return customAmbientSmoothness; }
            set
            {
                customAmbientSmoothness = value;
                Dirty = true;
            }
        }
        public float AmbientSmoothness
        {
            get { return customAmbientSmoothness ? ambientSmoothness : smoothness; }
            set
            {
                ambientSmoothness = value;
                Dirty = true;
            }
        }
        public float IsotropicScatteringIntensity
        {
            get { return isotropicScatteringIntensity; }
            set
            {
                isotropicScatteringIntensity = value;
                Dirty = true;
            }
        }
        public float ForwardScatteringIntensity
        {
            get { return forwardScatteringIntensity; }
            set
            {
                forwardScatteringIntensity = value;
                Dirty = true;
            }
        }
        public float SubsurfaceScatteringContrast
        {
            get { return subsurfaceScatteringContrast; }
            set
            {
                subsurfaceScatteringContrast = value;
                Dirty = true;
            }
        }

        public Color SubsurfaceScatteringShoreColor
        {
            get { return subsurfaceScatteringShoreColor; }
            set
            {
                subsurfaceScatteringShoreColor = value;
                Dirty = true;
            }
        }

        public float RefractionDistortion
        {
            get { return refractionDistortion; }
            set
            {
                refractionDistortion = value;
                Dirty = true;
            }
        }
        public float FresnelBias
        {
            get { return fresnelBias; }
            set
            {
                fresnelBias = value;
                Dirty = true;
            }
        }
        public float DetailFadeDistance
        {
            get { return detailFadeDistance * detailFadeDistance; }
            set
            {
                detailFadeDistance = value;
                Dirty = true;
            }
        }
        public float DisplacementNormalsIntensity
        {
            get { return displacementNormalsIntensity; }
            set
            {
                displacementNormalsIntensity = value;
                Dirty = true;
            }
        }
        public float PlanarReflectionIntensity
        {
            get { return planarReflectionIntensity; }
            set
            {
                planarReflectionIntensity = value;
                Dirty = true;
            }
        }
        public float PlanarReflectionFlatten
        {
            get { return planarReflectionFlatten; }
            set
            {
                planarReflectionFlatten = value;
                Dirty = true;
            }
        }
        public float PlanarReflectionVerticalOffset
        {
            get { return planarReflectionVerticalOffset; }
            set
            {
                windSpeed = value;
                Dirty = true;
            }
        }
        public float EdgeBlendFactor
        {
            get { return edgeBlendFactor; }
            set
            {
                edgeBlendFactor = value;
                Dirty = true;
            }
        }
        public float DirectionalWrapSSS
        {
            get { return directionalWrapSSS; }
            set
            {
                directionalWrapSSS = value;
                Dirty = true;
            }
        }
        public float PointWrapSSS
        {
            get { return pointWrapSSS; }
            set
            {
                pointWrapSSS = value;
                Dirty = true;
            }
        }
        public float DynamicSmoothnessIntensity
        {
            get { return dynamicSmoothnessIntensity; }
            set
            {
                dynamicSmoothnessIntensity = value;
                Dirty = true;
            }
        }
        public float Density
        {
            get { return density; }
            set
            {
                density = value;
                Dirty = true;
            }
        }
        public float UnderwaterBlurSize
        {
            get { return underwaterBlurSize; }
            set
            {
                underwaterBlurSize = value;
                Dirty = true;
            }
        }
        public float UnderwaterLightFadeScale
        {
            get { return underwaterLightFadeScale; }
            set
            {
                underwaterLightFadeScale = value;
                Dirty = true;
            }
        }
        public float UnderwaterDistortionsIntensity
        {
            get { return underwaterDistortionsIntensity; }
            set
            {
                underwaterDistortionsIntensity = value;
                Dirty = true;
            }
        }
        public float UnderwaterDistortionAnimationSpeed
        {
            get { return underwaterDistortionAnimationSpeed; }
            set
            {
                underwaterDistortionAnimationSpeed = value;
                Dirty = true;
            }
        }

        public NormalMapAnimation NormalMapAnimation1
        {
            get { return normalMapAnimation1; }
            set
            {
                normalMapAnimation1 = value;
                Dirty = true;
            }
        }
        public NormalMapAnimation NormalMapAnimation2
        {
            get { return normalMapAnimation2; }
            set
            {
                normalMapAnimation2 = value;
                Dirty = true;
            }
        }

        public float FoamIntensity
        {
            get { return foamIntensity; }
            set
            {
                foamIntensity = value;
                Dirty = true;
            }
        }
        public float FoamThreshold
        {
            get { return foamThreshold; }
            set
            {
                foamThreshold = value;
                Dirty = true;
            }
        }
        public float FoamFadingFactor
        {
            get { return foamFadingFactor; }
            set
            {
                foamFadingFactor = value;
                Dirty = true;
            }
        }
        public float FoamShoreIntensity
        {
            get { return foamShoreIntensity; }
            set
            {
                foamShoreIntensity = value;
                Dirty = true;
            }
        }
        public float FoamShoreExtent
        {
            get { return foamShoreExtent; }
            set
            {
                foamShoreExtent = value;
                Dirty = true;
            }
        }
        public float FoamNormalScale
        {
            get { return foamNormalScale; }
            set
            {
                foamNormalScale = value;
                Dirty = true;
            }
        }

        public Color FoamDiffuseColor
        {
            get { return foamDiffuseColor; }
            set
            {
                foamDiffuseColor = value;
                Dirty = true;
            }
        }
        public Color FoamSpecularColor
        {
            get { return foamSpecularColor; }
            set
            {
                foamSpecularColor = value;
                Dirty = true;
            }
        }

        public float SprayThreshold
        {
            get { return sprayThreshold; }
            set
            {
                sprayThreshold = value;
                Dirty = true;
            }
        }
        public float SpraySkipRatio
        {
            get { return spraySkipRatio; }
            set
            {
                spraySkipRatio = value;
                Dirty = true;
            }
        }

        public float SpraySize
        {
            get { return spraySize; }
            set
            {
                spraySize = value;
                Dirty = true;
            }
        }
        public Texture2D NormalMap
        {
            get { return normalMap; }
            set
            {
                normalMap = value;
                Dirty = true;
            }
        }
        public Texture2D FoamDiffuseMap
        {
            get { return foamDiffuseMap; }
            set
            {
                foamDiffuseMap = value;
                Dirty = true;
            }
        }
        public Texture2D FoamNormalMap
        {
            get { return foamNormalMap; }
            set
            {
                foamNormalMap = value;
                Dirty = true;
            }
        }

        public Vector2 FoamTiling
        {
            get { return foamTiling; }
            set
            {
                foamTiling = value;
                Dirty = true;
            }
        }
        public float WavesFrequencyScale
        {
            get { return wavesFrequencyScale; }
            set
            {
                wavesFrequencyScale = value;
                Dirty = true;
            }
        }

        public Gradient AbsorptionColorByDepth
        {
            get { return customUnderwaterAbsorptionColor ? absorptionColorByDepth : absorptionColorByDepthFlatGradient; }
            set
            {
                absorptionColorByDepth = value;
                Dirty = true;
            }
        }
        #endregion Public Variables

        #region Public Methods
        public void Synchronize()
        {
            Copy(Template);
        }

        public void Copy(WaterProfileData other)
        {
            spectrum = other.spectrum;
            spectrumType = other.spectrumType;

            windSpeed = other.windSpeed;
            tileSize = other.tileSize;
            tileScale = other.tileScale;
            wavesAmplitude = other.wavesAmplitude;
            wavesFrequencyScale = other.wavesFrequencyScale;
            horizontalDisplacementScale = other.horizontalDisplacementScale;
            phillipsCutoffFactor = other.phillipsCutoffFactor;
            gravity = other.gravity;
            fetch = other.fetch;
            directionality = other.directionality;
            absorptionColor = other.absorptionColor;
            customUnderwaterAbsorptionColor = other.customUnderwaterAbsorptionColor;
            absorptionColorByDepth = other.absorptionColorByDepth;
            absorptionColorByDepthFlatGradient = other.absorptionColorByDepthFlatGradient;
            diffuseColor = other.diffuseColor;
            specularColor = other.specularColor;
            depthColor = other.depthColor;
            emissionColor = other.emissionColor;
            reflectionColor = other.reflectionColor;
            smoothness = other.smoothness;
            customAmbientSmoothness = other.customAmbientSmoothness;
            ambientSmoothness = other.ambientSmoothness;
            isotropicScatteringIntensity = other.isotropicScatteringIntensity;
            forwardScatteringIntensity = other.forwardScatteringIntensity;
            subsurfaceScatteringContrast = other.subsurfaceScatteringContrast;
            subsurfaceScatteringShoreColor = other.subsurfaceScatteringShoreColor;
            refractionDistortion = other.refractionDistortion;
            fresnelBias = other.fresnelBias;
            detailFadeDistance = other.detailFadeDistance;
            displacementNormalsIntensity = other.displacementNormalsIntensity;
            planarReflectionIntensity = other.planarReflectionIntensity;
            planarReflectionFlatten = other.planarReflectionFlatten;
            planarReflectionVerticalOffset = other.planarReflectionVerticalOffset;
            edgeBlendFactor = other.edgeBlendFactor;
            directionalWrapSSS = other.directionalWrapSSS;
            pointWrapSSS = other.pointWrapSSS;
            density = other.density;
            underwaterBlurSize = other.underwaterBlurSize;
            underwaterLightFadeScale = other.underwaterLightFadeScale;
            underwaterDistortionsIntensity = other.underwaterDistortionsIntensity;
            underwaterDistortionAnimationSpeed = other.underwaterDistortionAnimationSpeed;
            dynamicSmoothnessIntensity = other.dynamicSmoothnessIntensity;
            normalMapAnimation1 = other.normalMapAnimation1;
            normalMapAnimation2 = other.normalMapAnimation2;
            normalMap = other.normalMap;
            foamIntensity = other.foamIntensity;
            foamThreshold = other.foamThreshold;
            foamFadingFactor = other.foamFadingFactor;
            foamShoreIntensity = other.foamShoreIntensity;
            foamShoreExtent = other.foamShoreExtent;
            foamNormalScale = other.foamNormalScale;
            foamDiffuseColor = other.foamDiffuseColor;
            foamSpecularColor = other.foamSpecularColor;
            sprayThreshold = other.sprayThreshold;
            spraySkipRatio = other.spraySkipRatio;
            spraySize = other.spraySize;
            foamDiffuseMap = other.foamDiffuseMap;
            foamNormalMap = other.foamNormalMap;
            foamTiling = other.foamTiling;
        }
        #endregion Public Methods

        #region Private Methods
        private void CreateSpectrum()
        {
            switch (spectrumType)
            {
                case WaterSpectrumType.Unified:
                {
                    spectrum = new UnifiedSpectrum(tileSize, -gravity, windSpeed, wavesAmplitude, wavesFrequencyScale, fetch);
                    break;
                }

                case WaterSpectrumType.Phillips:
                {
                    spectrum = new PhillipsSpectrum(tileSize, -gravity, windSpeed, wavesAmplitude, phillipsCutoffFactor);
                    break;
                }
            }
        }
        #endregion Private Methods
    }
}