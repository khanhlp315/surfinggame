﻿using UltimateWater.Utils;

namespace UltimateWater
{
    using UnityEngine;
    using Internal;

    public class WaterDropsIME : MonoBehaviour, IWaterImageEffect
    {
        #region Public Types
        public enum Type
        {
            NormalMap,
            Blur
        }

        public interface IWaterDropsModule
        {
            void Initialize(WaterDropsIME ime);

            void Validate();
            void Advance();

            void UpdateMask(RenderTexture mask);
            void RenderImage(RenderTexture source, RenderTexture destination);
        }

        [System.Serializable]
        public struct BlurModule : IWaterDropsModule
        {
            #region Public Variables
            public float FadeSpeed;
            #endregion Public Variables

            #region Public Methods
            public void Initialize(WaterDropsIME effect)
            {
                _Reference = effect;
                _Camera = effect.GetComponent<WaterCamera>();
            }
            public void Validate()
            {
                _Blur.Validate("UltimateWater/Utilities/Blur (VisionBlur)");
            }

            public void Advance()
            {
                if (!Application.isPlaying)
                {
                    return;
                }

                _Intensity += Mathf.Max(0.0f, _Camera.WaterLevel - _Reference.transform.position.y);
                _Intensity *= 1.0f - Time.deltaTime * FadeSpeed;

                _Intensity = Mathf.Clamp01(_Intensity);
            }

            public void RenderImage(RenderTexture source, RenderTexture destination)
            {
                float blurSize = _Blur.Size;
                _Blur.Size *= _Intensity;
                _Blur.Apply(source);
                _Blur.Size = blurSize;

                Graphics.Blit(source, destination);
            }
            #endregion Public Methods

            #region Private Variables
            private WaterDropsIME _Reference;
            private WaterCamera _Camera;
            private float _Intensity;

            [HideInInspector] [SerializeField] private Blur _Blur;
            #endregion Private Variables

            #region Unused Interfaces
            public void UpdateMask(RenderTexture mask)
            {
            }
            #endregion Unused Interfaces
        }

        [System.Serializable]
        public struct NormalModule : IWaterDropsModule
        {
            #region Public Variables
            public Texture NormalMap;
            public float Intensity;
            public bool Preview;
            #endregion Public Variables

            #region Public Methods
            public void Initialize(WaterDropsIME effect)
            {
                _Material = ShaderUtility.Instance.CreateMaterial(ShaderList.WaterdropsNormal);
                _Material.SetTexture("_NormalMap", NormalMap);
            }

            public void UpdateMask(RenderTexture mask)
            {
                if (Preview)
                {
                    Graphics.Blit(TextureUtility.WhiteTexture, mask);
                }

                _Material.SetTexture("_Mask", mask);
            }
            public void RenderImage(RenderTexture source, RenderTexture destination)
            {
                if (!Application.isPlaying && !Preview)
                {
                    Graphics.Blit(source, destination);
                    return;
                }

                _Material.SetFloat("_Intensity", Intensity);
                Graphics.Blit(source, destination, _Material);
            }

            public void Validate()
            {
                ShaderUtility.Instance.Use(ShaderList.WaterdropsNormal);

                if (_Material == null)
                {
                    _Material = ShaderUtility.Instance.CreateMaterial(ShaderList.WaterdropsNormal);
                }
                _Material.SetTexture("_NormalMap", NormalMap);
            }
            #endregion Public Methods

            #region Private Variables
            private Material _Material;
            #endregion Private Variables

            #region Unused Interfaces
            public void Advance()
            {
            }
            #endregion Unused Interfaces
        }
        #endregion Public Types

        #region Inspector Variables
        [SerializeField] private Type _Type = Type.NormalMap;
        #endregion Inspector Variables

        #region Public Variables
        public RenderTexture Mask
        {
            get { return _Masking.MaskB; }
        }

        [Range(0.95f, 1.0f)]
        [Tooltip("How slow the effects disappear")]
        public float Fade = 1.0f;

        public BlurModule Blur;
        public NormalModule Normal;
        #endregion Public Variables

        #region Unity Messages
        private void Awake()
        {
            _Masking.Material = ShaderUtility.Instance.CreateMaterial(ShaderList.WaterdropsMask);

            AssignModule();
        }

        private void OnValidate()
        {
            ShaderUtility.Instance.Use(ShaderList.WaterdropsMask);

            AssignModule();

            _SelectedModule.Validate();
            if (Application.isPlaying && _Masking.Material != null)
            {
                _Masking.Material.SetFloat("_Fadeout", Fade * 0.98f);
            }
        }

        private void OnPreCull()
        {
            _SelectedModule.Advance();
        }
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            if (Application.isPlaying)
            {
                CheckResources();

                _Masking.Blit();
                _Masking.Swap();

                _SelectedModule.UpdateMask(_Masking.MaskB);
            }

            _SelectedModule.RenderImage(source, destination);
        }
        private void Reset()
        {
            Normal.Intensity = 1.0f;
            Blur.FadeSpeed = 1.0f;
        }
        #endregion Unity Messages

        #region Private Types
        private struct MaskingModule
        {
            #region Public Variables
            internal Material Material;
            internal RenderTexture MaskA;
            internal RenderTexture MaskB;
            #endregion Public Variables

            #region Public Methods
            internal void Blit()
            {
                Graphics.Blit(MaskA, MaskB, Material, 0);
            }
            internal void Swap()
            {
                var temp = MaskA;
                MaskA = MaskB;
                MaskB = temp;
            }
            #endregion Public Methods
        }
        #endregion Private Types

        #region Private Variables
        private MaskingModule _Masking;
        private IWaterDropsModule _SelectedModule;
        #endregion Private Variables

        #region Private Methods
        private void AssignModule()
        {
            switch (_Type)
            {
                case Type.Blur: _SelectedModule = Blur; break;
                case Type.NormalMap: _SelectedModule = Normal; break;
            }
            _SelectedModule.Initialize(this);
        }

        private void CheckResources()
        {
            if (_Masking.MaskA == null || _Masking.MaskA.width != Screen.width >> 1 || _Masking.MaskA.height != Screen.height >> 1)
            {
                _Masking.MaskA = CreateMaskRT();
                _Masking.MaskB = CreateMaskRT();
            }
        }

        private static RenderTexture CreateMaskRT()
        {
            var renderTexture = new RenderTexture(Screen.width >> 1, Screen.height >> 1, 0, RenderTextureFormat.RHalf, RenderTextureReadWrite.Linear)
            {
                hideFlags = HideFlags.DontSave,
                filterMode = FilterMode.Bilinear
            };

            Graphics.SetRenderTarget(renderTexture);
            GL.Clear(false, true, Color.black);

            return renderTexture;
        }
        #endregion Private Methods

        public void OnWaterCameraEnabled()
        {
        }

        public void OnWaterCameraPreCull()
        {
        }

        #region Validation
        [InspectorWarning("Validation", InspectorWarningAttribute.InfoType.Warning)]
        [SerializeField]
        private string _Validation;
        private string Validation()
        {
            string info = string.Empty;
            if (_Type == Type.NormalMap && Normal.NormalMap == null)
            {
                info += "warning: select normal map texture";
            }

            return info;
        }
        #endregion Validation
    }
}