﻿using UnityEngine;
using UnityEngine.Rendering;

namespace UltimateWater
{
    public sealed class WaterShadowCastingLight : MonoBehaviour
    {
        #region Unity Messages
        private void Start()
        {
            int shadowmapId = Shader.PropertyToID("_WaterShadowmap");

            _CommandBuffer = new CommandBuffer { name = "Water: Copy Shadowmap" };
            _CommandBuffer.GetTemporaryRT(shadowmapId, Screen.width, Screen.height, 32, FilterMode.Point,
                RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
            _CommandBuffer.Blit(BuiltinRenderTextureType.CurrentActive, shadowmapId);

            var light = GetComponent<Light>();
            light.AddCommandBuffer(LightEvent.AfterScreenspaceMask, _CommandBuffer);
        }
        #endregion Unity Messages

        #region Private Variables
        private CommandBuffer _CommandBuffer;
        #endregion Private Variables
    }
}