﻿namespace UltimateWater.Internal
{
    using System.Collections.Generic;
    using UnityEngine;
    using Utils;

#if UNITY_EDITOR

    using UnityEditor;

    [InitializeOnLoad]
#endif
    public class Compatibility
    {
        #region Public Methods
        static Compatibility()
        {
            const string key = "[Ultimate Water]: compatibility check";

            if (PlayerPrefs.GetInt(key) == Versioning.Number) return;
            Formats();
            PlayerPrefs.SetInt(key, Versioning.Number);
        }

        public static RenderTextureFormat? GetFormat(RenderTextureFormat preferred, IEnumerable<RenderTextureFormat> fallback = null)
        {
            if (IsFormatSupported(preferred)) return preferred;
            if (fallback == null) return null;

            foreach (var format in fallback)
            {
                if (SystemInfo.SupportsRenderTextureFormat(format))
                {
                    return format;
                }
            }

            return null;
        }
        #endregion Public Methods

        #region Private Methods
        private static void Formats()
        {
            var result = true;
            result &= IsFormatSupported(RenderTextureFormat.ARGBFloat);
            result &= IsFormatSupported(RenderTextureFormat.ARGBHalf);
            result &= IsFormatSupported(RenderTextureFormat.ARGB32);

            result &= IsFormatSupported(RenderTextureFormat.RGHalf);

            result &= IsFormatSupported(RenderTextureFormat.RFloat);
            result &= IsFormatSupported(RenderTextureFormat.RHalf);
            result &= IsFormatSupported(RenderTextureFormat.R8);

            result &= IsFormatSupported(RenderTextureFormat.Depth);

            if (result)
            {
                Debug.Log("[Ultimate Water]: all necessary render texture formats supported");
            }
            else
            {
                Debug.LogError("[Ultimate Water]: some of the necessary render texture formats not supported,\n" +
                               "some features will not be available");
            }
        }
        #endregion Private Methods

        #region Helper Methods
        private static bool IsFormatSupported(RenderTextureFormat format)
        {
            var supports = SystemInfo.SupportsRenderTextureFormat(format);
            return supports;
        }
        #endregion Helper Methods
    }
}