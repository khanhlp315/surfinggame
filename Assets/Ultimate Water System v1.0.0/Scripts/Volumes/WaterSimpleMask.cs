﻿namespace UltimateWater.Internal
{
    using UnityEngine;

    /// <summary>
    ///     Attach this to objects supposed to mask water in screen-space. It will mask both water surface and camera's
    ///     underwater image effect. Great for sections etc.
    /// </summary>
    [RequireComponent(typeof(Renderer))]
    public sealed class WaterSimpleMask : MonoBehaviour
    {
        [SerializeField] private Water water;
        [SerializeField] private int renderQueuePriority;

        private Renderer rendererComponent;

        private void OnEnable()
        {
            rendererComponent = GetComponent<Renderer>();
            rendererComponent.enabled = false;
            rendererComponent.material.SetFloat("_WaterId", 1 << water.WaterId);

            gameObject.layer = WaterProjectSettings.Instance.WaterTempLayer;

            if (rendererComponent == null)
                throw new System.InvalidOperationException("WaterSimpleMask is attached to an object without any renderer.");

            water.Renderer.AddMask(this);
            water.WaterIdChanged += OnWaterIdChanged;
        }

        private void OnDisable()
        {
            water.WaterIdChanged -= OnWaterIdChanged;
            water.Renderer.RemoveMask(this);
        }

        public Renderer Renderer
        {
            get { return rendererComponent; }
        }

        public int RenderQueuePriority
        {
            get { return renderQueuePriority; }
        }

        public Water Water
        {
            get { return water; }
            set
            {
                if (water == value)
                    return;

                enabled = false;
                water = value;
                enabled = true;
            }
        }

        private void OnValidate()
        {
            gameObject.layer = WaterProjectSettings.Instance.WaterTempLayer;
        }

        private void OnWaterIdChanged()
        {
            var renderer = GetComponent<Renderer>();
            renderer.material.SetFloat("_WaterId", 1 << water.WaterId);
        }
    }
}