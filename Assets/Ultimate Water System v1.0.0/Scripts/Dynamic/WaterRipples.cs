﻿using System.Globalization;

namespace UltimateWater
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Water ripples settings,
    /// used for adding forces to simulations
    /// passes rendered screen space textures to water render shader
    /// </summary>
    public class WaterRipples : MonoBehaviour
    {
        #region Public Methods
        // Add new WaterSimulationArea to simulation
        public static void Register(WaterSimulationArea area)
        {
            var ripples = Instance;
            if (ripples == null)
            {
                return;
            }

            if (ripples._Areas.Contains(area))
            {
                return;
            }
            ripples._Areas.Add(area);

            area.UpdateShaderVariables();
        }

        // Remove WaterSimulationArea from simulation
        public static void Unregister(WaterSimulationArea area)
        {
            var ripples = Instance;
            if (ripples == null)
            {
                return;
            }
            ripples._Areas.Remove(area);
        }

        // Adds force to simulation area containing provided position
        public static void AddForce(List<WaterForceData> data, float radius = 1.0f)
        {
            var ripples = Instance;
            if (ripples == null)
            {
                return;
            }

            for (int i = 0; i < ripples._Areas.Count; ++i)
            {
                var entry = ripples._Areas[i];
                entry.AddForce(data, radius);
            }
        }
        #endregion Public Methods

        #region Unity Messages
        private void FixedUpdate()
        {
            if (Time.timeScale == 0.0f)
            {
                return;
            }
            Simulate();
        }

        private void OnDisable()
        {
            _Instance = null;
            _Areas.Clear();
        }
        private void OnApplicationQuit()
        {
            _Quiting = true;
        }
        #endregion Unity Messages

        #region Private Variables
        private static WaterRipples Instance
        {
            get
            {
                if (_Quiting)
                {
                    return null;
                }
                if (_Instance == null)
                {
                    var obj = new GameObject("Water Ripples Simulator") { hideFlags = HideFlags.HideInHierarchy };
                    _Instance = obj.AddComponent<WaterRipples>();
                }
                return _Instance;
            }
        }

        private static bool _Quiting;
        private static WaterRipples _Instance;

        private readonly List<WaterSimulationArea> _Areas = new List<WaterSimulationArea>();
        #endregion Private Variables

        #region Private Methods
        // Progresses all simulation areas
        private void Simulate()
        {
            for (int i = 0; i < WaterQualitySettings.Instance.Ripples.Iterations; ++i)
            {
                for (int index = 0; index < _Areas.Count; ++index)
                {
                    _Areas[index].Simulate();
                }

                for (int index = 0; index < _Areas.Count; ++index)
                {
                    _Areas[index].Smooth();
                }

                for (int index = 0; index < _Areas.Count; ++index)
                {
                    _Areas[index].Swap();
                }
            }
        }
        #endregion Private Methods
    }
}