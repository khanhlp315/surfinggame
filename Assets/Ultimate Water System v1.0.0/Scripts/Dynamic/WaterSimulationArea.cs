﻿namespace UltimateWater
{
#if UNITY_EDITOR

    using UnityEditor;

#endif

    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.Rendering;
    using Internal;
    using System.Collections;

    public struct WaterForceData
    {
        public Vector3 Position;
        public float Force;
    }

    /// <summary>
    /// Simulates wave propagation in defined space
    /// </summary>
    [RequireComponent(typeof(MeshFilter))]
    [AddComponentMenu("Ultimate Water/Dynamic/Water Simulation Area")]
    public class WaterSimulationArea : MonoBehaviour, ILocalDisplacementRenderer
    {
        #region Inspector Variables

        public Water Water;

        public WaterRipplesProfile Profile
        {
            get { return _Profile; }
            set
            {
                _Profile = value;
                UpdateShaderVariables();
            }
        }

        [SerializeField]
        private WaterRipplesProfile _Profile;

        [Header("Settings")]
        [Tooltip("How many simulation pixels per one unit are used")]
        [SerializeField]
        [Range(1, 32)]
        private int _PixelsPerUnit = 16;

        [Tooltip("What resolution is used for dynamic depth rendering")]
        [SerializeField]
        [Range(0.125f, 2.0f)]
        private float _DepthScale = 0.5f;

        [Tooltip("Does the water can be  stopped by Blocking objects")]
        [SerializeField]
        private bool _EnableStaticCalculations = false;

        [Tooltip("What resolution is used for static depth information")]
        [SerializeField]
        [Range(0.125f, 2.0f)]
        private float _StaticDepthScale = 1.0f;

        [Header("Edge fade")]
        [SerializeField]
        private bool _Fade;

        [SerializeField]
        private float _FadePower = 0.5f;

        #endregion Inspector Variables

        #region Public Variables

        // Called before each simulation iteration
        public int PixelsPerUnit
        {
            get
            {
                return _PixelsPerUnit;
            }
            set
            {
                _PixelsPerUnit = value;
            }
        }

        // Simulation resolution in pixels
        public Vector2 Resolution
        {
            get
            {
                var result = Size * _PixelsPerUnit;
                return new Vector2((int)result.x, (int)result.y);
            }
        }

        // Simulation size in units
        public Vector2 Size
        {
            get
            {
                return new Vector2(transform.localScale.x * 10.0f, transform.localScale.z * 10.0f);
            }
            set
            {
                transform.localScale = new Vector3(value.x * 0.1f, 1.0f, value.y * 0.1f);
            }
        }

        public Vector2 DepthResolution
        {
            get
            {
                return Resolution * _DepthScale;
            }
        }

        // Simulation matrices
        public RenderTexture Primary
        {
            get
            {
                return _Buffers[1];
            }
        }

        public RenderTexture Secondary
        {
            get
            {
                return _Buffers[0];
            }
        }

        #endregion Public Variables

        #region Public Methods

        // Checks if point is inside simulation area
        public bool Contains(Vector3 globalPosition)
        {
            var local = GetLocalPixelPosition(globalPosition);

            return local.x >= 0 && local.x < Resolution.x &&
                   local.y >= 0 && local.y < Resolution.y;
        }

        // Transforms global position into pixel coordinates on simulation matrices
        public Vector2 GetLocalPixelPosition(Vector3 globalPosition)
        {
            // Get the 2d position
            var position = new Vector2(transform.position.x, transform.position.z);

            // Translate to the left-bottom corner - [0, 0]px
            var result = (position - new Vector2(globalPosition.x, globalPosition.z)) + Size * 0.5f;

            // Scale from global to pixel units
            result.x *= _PixelsPerUnit;
            result.y *= _PixelsPerUnit;
            return result;
        }

        // Adds force to simulation matrix
        public void AddForce(List<WaterForceData> data, float radius = 1.0f)
        {
            var resolution = Resolution;
            int selected = 0;

            for (int i = 0; (i < data.Count) && (selected < 512); ++i)
            {
                var position = GetLocalPixelPosition(data[i].Position);
                if (!ContainsLocalRaw(position, resolution))
                {
                    continue;
                }

                _Array[selected * 4] = position.x;
                _Array[selected * 4 + 1] = position.y;
                _Array[selected * 4 + 2] = data[i].Force * 500.0f;
                _Array[selected * 4 + 3] = 0.0f;

                selected++;
            }

            if (selected == 0)
            {
                return;
            }

            var shader = SetupShader.Shader;

            shader.SetTexture(SetupShader.Multi, SetupShader.PrimaryName, _Buffers[0]);
            shader.SetTexture(SetupShader.Multi, SetupShader.SecondaryName, _Buffers[1]);

            shader.SetFloats("data", _Array);

            shader.Dispatch(SetupShader.Multi, selected, 1, 1);
        }

        // Progresses simulation by one tick
        public void Simulate()
        {
            // we need to clear both buffers even if only one RT was recreated
            if (TextureUtility.Verify(_Buffers[0], false) || TextureUtility.Verify(_Buffers[1], false))
            {
                Graphics.SetRenderTarget(_Buffers[0]);
                GL.Clear(false, true, Color.clear);
                Graphics.SetRenderTarget(_Buffers[1]);
                GL.Clear(false, true, Color.clear);
                Graphics.SetRenderTarget(null);
            }

            // Notify about next iteration is about to happen
            PreIteration.Invoke();

            // Set all necessary shader data
            PreSimulate();

            // Based on that, simulate in pixel or compute shader
            var mode = WaterQualitySettings.Instance.Ripples.ShaderMode;
            switch (mode)
            {
                case WaterRipplesData.ShaderModes.ComputeShader:
                    {
                        SimulateCS();
                        break;
                    }
                case WaterRipplesData.ShaderModes.PixelShader:
                    {
                        SimulatePS();
                        break;
                    }
            }
        }

        public void Smooth()
        {
            if (Profile.Sigma <= 0.0f)
            {
                return;
            }

            var terms = Utils.Math.GaussianTerms(Profile.Sigma);
            GaussianShader.Term0 = terms[0];
            GaussianShader.Term1 = terms[1];
            GaussianShader.Term2 = terms[2];

            // Get temporary render texture
            var temp = RenderTexturesCache.GetTemporary(_Width, _Height, 0, WaterQualitySettings.Instance.Ripples.SimulationFormat,
                true, true);

            // Ensure that temp is created
            TextureUtility.Verify(temp);

            // Blur simulation matrix
            {
                // Vertical Pass
                GaussianShader.VerticalInput = _Buffers[1];
                GaussianShader.VerticalOutput = temp;
                GaussianShader.Dispatch(GaussianShader.KernelType.Vertical, _Width, _Height);

                // Horizontal pass
                GaussianShader.HorizontalInput = temp;
                GaussianShader.HorizontalOutput = _Buffers[1];
                GaussianShader.Dispatch(GaussianShader.KernelType.Horizontal, _Width, _Height);
            }

            // Release temporary
            temp.Dispose();
        }

        public void Swap()
        {
            TextureUtility.Swap(ref _Buffers[0], ref _Buffers[1]);
        }

        // Send shader variables from WaterRipples to shaders
        public void UpdateShaderVariables()
        {
            if (Profile == null)
            {
                return;
            }

            RipplesShader.SetDamping(Profile.Damping / 32.0f, _RipplesMaterial);
            RipplesShader.SetPropagation(Profile.Propagation, _RipplesMaterial);

            RipplesShader.SetGain(Profile.Gain, _RipplesMaterial);

            RipplesShader.SetHeightGain(Profile.HeightGain, _RipplesMaterial);
            RipplesShader.SetHeightOffset(Profile.HeightOffset, _RipplesMaterial);

            _DisplacementMaterial.SetFloat("_Amplitude", Profile.Amplitude * 0.05f);
            _DisplacementMaterial.SetFloat("_Spread", Profile.Spread);
            _DisplacementMaterial.SetFloat("_Multiplier", Profile.Multiplier);
        }

        public void Reload()
        {
            OnDisable();
            OnEnable();
        }

        #endregion Public Methods

        #region Private Variables

        [HideInInspector]
        internal UnityEvent PreIteration = new UnityEvent();

        private Camera _DepthCamera;

        private readonly RenderTexture[] _Buffers = new RenderTexture[2];

        private RenderTexture _PreviousDepth;
        private RenderTexture _Depth;

        private RenderTexture _StaticDepth;

        private Material _RipplesMaterial
        {
            get
            {
                if (_RipplesMaterialCache == null)
                {
                    var shader = ShaderUtility.Instance.Get(ShaderList.Simulation);
                    _RipplesMaterialCache = new Material(shader);
                }

                return _RipplesMaterialCache;
            }
        }

        private Material _RipplesMaterialCache;

        private MeshFilter _MeshFilter;

        [SerializeField]
        [HideInInspector]
        private Material _DisplacementMaterial;

        private int _Width
        {
            get { return (int)Resolution.x; }
        }

        private int _Height
        {
            get { return (int)Resolution.y; }
        }

        private int _DepthWidth
        {
            get { return (int)(Resolution.x * _DepthScale); }
        }

        private int _DepthHeight
        {
            get { return (int)(Resolution.y * _DepthScale); }
        }

        private int _StaticWidth
        {
            get { return (int)(Resolution.x * _StaticDepthScale); }
        }

        private int _StaticHeight
        {
            get { return (int)(Resolution.y * _StaticDepthScale); }
        }

        private Vector3 _Position;
        private Vector3 _Scale;

        private Material _Material;
        private CommandBuffer _CommandBuffer;

        // max values allowed by setup shader [512 * float4]
        private static readonly float[] _Array = new float[512 * 4];

        #endregion Private Variables

        #region Unity Messages

        private void Awake()
        {
            // assing references
            _MeshFilter = GetComponent<MeshFilter>();
            _Material = new Material(ShaderUtility.Instance.Get(ShaderList.Translate));
        }

        private IEnumerator Start()
        {
            yield return null;
            // Wait for all textures to be created
            RenderStaticDepth();
        }

        private void OnEnable()
        {
            _Scale = transform.localScale;
            Reset();
            RefreshFade();

            _CommandBuffer = new CommandBuffer { name = "[Ultimate Water]: Water Simulation Area" };
            _Position = transform.position;

            CreateTextures();
            InitializeDepth();
            RenderStaticDepth();

            DynamicWater.AddRenderer(this);
            WaterRipples.Register(this);
        }

        private void OnDisable()
        {
            WaterRipples.Unregister(this);
            DynamicWater.RemoveRenderer(this);

            ReleaseDepthCamera();
            ReleaseTextures();

            _CommandBuffer = null;
        }

        private void Update()
        {
            Shader.SetGlobalFloat("_WaterHeight", Water.transform.position.y);
            transform.position = new Vector3(transform.position.x, Water.transform.position.y, transform.position.z);
            transform.rotation = Quaternion.identity;

            transform.localScale = new Vector3(_Scale.x, 1.0f, _Scale.z);

            RenderDepth();
        }

        private void OnValidate()
        {
            ShaderUtility.Instance.Use(ShaderList.Simulation);
            ShaderUtility.Instance.Use(ShaderList.Translate);
            ShaderUtility.Instance.Use(ShaderList.Velocity);
            ShaderUtility.Instance.Use(ShaderList.Depth);

            if (Application.isPlaying && _EnableStaticCalculations)
            {
                if (_StaticDepth == null)
                {
                    CreateStaticDepthTexture();
                }

                RenderStaticDepth();
            }
            else if (_StaticDepth != null)
            {
                _StaticDepth.Release();
                _StaticDepth = null;
            }

            RefreshFade();
        }

        private void Reset()
        {
            if (Water == null)
            {
                Water = Utilities.GetWaterReference();
            }

            // get a mesh plane >the long way<
            var go = GameObject.CreatePrimitive(PrimitiveType.Plane);

            var filter = GetComponent<MeshFilter>();
            filter.sharedMesh = go.GetComponent<MeshFilter>().sharedMesh;
            filter.hideFlags = HideFlags.HideInInspector;

            DestroyImmediate(go);

            // initialize overlay
            _DisplacementMaterial = new Material(Resources.Load<Material>("Materials/Overlay (Displacements)"));
        }

#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            // Draw area gizmo
            if (Camera.main == null || Water == null) return;

            var color = Color.cyan;
            // Draw cube representation
            Gizmos.color = color * new Color(1.0f, 1.0f, 1.0f, 0.6f);
            Gizmos.DrawWireCube(
                 new Vector3(transform.position.x, Water.transform.position.y, transform.position.z),
                 new Vector3(Size.x, 1.0f, Size.y));

            Gizmos.color = color * new Color(1.0f, 1.0f, 1.0f, 0.05f);
            Gizmos.DrawCube(
                 new Vector3(transform.position.x, Water.transform.position.y, transform.position.z),
                 new Vector3(Size.x, 1.0f, Size.y));
        }

        // Used by CustomEditor to refresh data
        public void Refresh()
        {
            if (!Application.isPlaying || !Application.isEditor) return;

            UpdateSimulationResolution();
            MatchCameraResolution(ref _DepthCamera);
        }

#endif

        #endregion Unity Messages

        #region Private Methods

        #region Simulation

        private void PreSimulate()
        {
            UpdateDynamic();

            // Setup ping-pong buffers
            RipplesShader.SetPrimary(_Buffers[0], _RipplesMaterial);
            RipplesShader.SetSecondary(_Buffers[1], _RipplesMaterial);

            // Set depth data
            RipplesShader.SetDepth(_Depth, _RipplesMaterial);
            RipplesShader.SetPreviousDepth(_PreviousDepth, _RipplesMaterial);

            if (_EnableStaticCalculations && _StaticDepth != null)
            {
                RipplesShader.SetStaticDepth(_StaticDepth, _RipplesMaterial);
            }
            else
            {
                RipplesShader.SetStaticDepth(TextureUtility.ClearTexture, _RipplesMaterial);
            }

            float scale = (float)_PixelsPerUnit / 32.0f;
            RipplesShader.SetPropagation(Profile.Propagation * scale, _RipplesMaterial);
        }

        private void SimulateCS()
        {
            // Set simulation parameters
            RipplesShader.Size = Resolution;

            // Tick simulation
            RipplesShader.Dispatch(_Width, _Height);
        }

        private void SimulatePS()
        {
            var temp = RenderTexture.GetTemporary(_Width, _Height, 0,
                   WaterQualitySettings.Instance.Ripples.SimulationFormat, RenderTextureReadWrite.Linear);

            TextureUtility.Clear(temp);

            Graphics.Blit(null, temp, _RipplesMaterial);
            Graphics.Blit(temp, _Buffers[1]);

            RenderTexture.ReleaseTemporary(temp);
        }

        private void UpdateSimulationResolution()
        {
            // check if resizing is needed
            if (_Width == _Buffers[0].width && _Height == _Buffers[0].height)
            {
                return;
            }

            Graphics.SetRenderTarget(null);
            var desc = new TextureUtility.RenderTextureDesc(_Buffers[0])
            {
                Width = _Width,
                Height = _Height
            };

            // Create new RenderTextures with new resolution
            var primary = TextureUtility.CreateRenderTexture(desc);
            var secondary = TextureUtility.CreateRenderTexture(desc);

            // Render old buffers onto the new ones
            Graphics.Blit(_Buffers[0], primary);
            Graphics.Blit(_Buffers[1], secondary);

            // Release old buffers
            TextureUtility.Release(ref _Buffers[0]);
            TextureUtility.Release(ref _Buffers[1]);

            // Replace the old buffers
            _Buffers[0] = primary;
            _Buffers[1] = secondary;

            RefreshMaterialsData();
        }

        #endregion Simulation

        #region Depth

        private void InitializeDepth()
        {
            _DepthCamera = CreateDepthCamera();
            SetDepthCameraParameters(ref _DepthCamera);
        }

        private void SetDepthCameraParameters(ref Camera camera,
                    float height = 10.0f,
                    float near = 0.0f, float far = 20.0f)
        {
            camera.nearClipPlane = near;
            camera.farClipPlane = far;
            camera.transform.localPosition = new Vector3(0.0f, height, 0.0f);
        }

        private Camera CreateDepthCamera()
        {
            if (Resolution.x <= 0 || Resolution.y <= 0)
            {
                Debug.LogError("WaterSimulationArea: invalid resolution");
                return null;
            }

            var go = new GameObject("Depth");
            go.transform.SetParent(transform);

            var camera = go.AddComponent<Camera>();
            camera.enabled = false;
            camera.backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
            camera.clearFlags = CameraClearFlags.Color;
            camera.orthographic = true;
            camera.orthographicSize = Size.y * 0.5f;
            camera.aspect = Resolution.x / Resolution.y;
            camera.transform.localRotation = Quaternion.Euler(new Vector3(90.0f, 180.0f, 0.0f));
            camera.transform.localScale = Vector3.one;
            camera.depthTextureMode = DepthTextureMode.Depth;
            return camera;
        }

        private void ReleaseDepthCamera()
        {
            if (_DepthCamera != null)
            {
                Destroy(_DepthCamera.gameObject);
            }
            _DepthCamera = null;
        }

        private void RenderDepth()
        {
            if (_DepthCamera == null)
            {
                return;
            }

            TextureUtility.Swap(ref _Depth, ref _PreviousDepth);

            var interactive = DynamicWater.Interactions;
            for (int i = 0; i < interactive.Count; ++i)
            {
                interactive[i].Enable();
            }

            // setup matrices
            GL.PushMatrix();
            GL.modelview = _DepthCamera.worldToCameraMatrix;
            GL.LoadProjectionMatrix(_DepthCamera.projectionMatrix);

            _CommandBuffer.Clear();

            _CommandBuffer.SetRenderTarget(_Depth);
            _CommandBuffer.ClearRenderTarget(true, true, Color.clear);

            for (int i = 0; i < interactive.Count; ++i)
            {
                interactive[i].Render(_CommandBuffer);
            }

            Graphics.ExecuteCommandBuffer(_CommandBuffer);
            GL.PopMatrix();

            for (int i = 0; i < interactive.Count; ++i)
            {
                interactive[i].Disable();
            }
        }

        private void RenderStaticDepth()
        {
            if (_DepthCamera == null) return;
            if (_EnableStaticCalculations == false) return;

            var height = _DepthCamera.transform.localPosition.y;
            var near = _DepthCamera.nearClipPlane;
            var far = _DepthCamera.farClipPlane;

            SetDepthCameraParameters(ref _DepthCamera, 40.0f, 0.0f, 80.0f);

            _DepthCamera.cullingMask = WaterQualitySettings.Instance.Ripples.StaticDepthMask;
            _DepthCamera.targetTexture = _StaticDepth;
            _DepthCamera.SetReplacementShader(ShaderUtility.Instance.Get(ShaderList.Depth), "");

            _DepthCamera.Render();

            SetDepthCameraParameters(ref _DepthCamera, height, near, far);
        }

        #endregion Depth

        #region Dynamic

        private void UpdateDynamic()
        {
            if (transform.position.x != _Position.x || transform.position.z != _Position.z)
            {
                RenderStaticDepth();
                MoveSimulation();
                _Position = transform.position;
            }
        }

        private void MoveSimulation()
        {
            var vector = _Position - transform.position;
            float x = vector.x * _PixelsPerUnit;
            float z = vector.z * _PixelsPerUnit;
            _Material.SetVector("_Offset", new Vector4(x, z, 0.0f, 0.0f));

            var simulationTemporary = TextureUtility.CreateTemporary(_Buffers[0]);
            {
                Graphics.Blit(_Buffers[0], simulationTemporary, _Material);
                Graphics.Blit(simulationTemporary, _Buffers[0]);

                Graphics.Blit(_Buffers[1], simulationTemporary, _Material);
                Graphics.Blit(simulationTemporary, _Buffers[1]);
            }
            RenderTexture.ReleaseTemporary(simulationTemporary);

            var depthTemporary = TextureUtility.CreateTemporary(_Depth);
            {
                TextureUtility.Clear(depthTemporary, true, false);

                Graphics.Blit(_Depth, depthTemporary, _Material);
                Graphics.Blit(depthTemporary, _Depth);

                Graphics.Blit(_PreviousDepth, depthTemporary, _Material);
                Graphics.Blit(depthTemporary, _PreviousDepth);
            }
            RenderTexture.ReleaseTemporary(depthTemporary);
        }

        #endregion Dynamic

        private bool ContainsLocalRaw(Vector2 localPosition, Vector2 resolution)
        {
            return localPosition.x >= 0 && localPosition.x < resolution.x &&
                   localPosition.y >= 0 && localPosition.y < resolution.y;
        }

        // Creates all necessary textures for simulation
        private void CreateTextures()
        {
            // Create descriptions
            var simulationDesc = new TextureUtility.RenderTextureDesc()
            {
                Width = _Width,
                Height = _Height,
                Format = WaterQualitySettings.Instance.Ripples.SimulationFormat,
                Filter = FilterMode.Bilinear,
                EnableRandomWrite = true
            };
            var depthDesc = new TextureUtility.RenderTextureDesc()
            {
                Width = _DepthWidth,
                Height = _DepthHeight,
                Depth = 24,
                Format = RenderTextureFormat.ARGBHalf,
                Filter = FilterMode.Point
            };

            // Create simulation textures
            _Buffers[0] = TextureUtility.CreateRenderTexture(simulationDesc);
            _Buffers[1] = TextureUtility.CreateRenderTexture(simulationDesc);

            // Create depth textures
            _Depth = TextureUtility.CreateRenderTexture(depthDesc);
            _PreviousDepth = TextureUtility.CreateRenderTexture(depthDesc);

            // Create static depth if enabled
            if (_EnableStaticCalculations)
            {
                CreateStaticDepthTexture();
            }

            // Those textures are "additive"
            // so we must assure that they are black
            TextureUtility.Clear(_Buffers[0]);
            TextureUtility.Clear(_Buffers[1]);

            RefreshMaterialsData();
        }

        private void CreateStaticDepthTexture()
        {
            var staticDepthDesc = new TextureUtility.RenderTextureDesc()
            {
                Width = _StaticWidth,
                Height = _StaticHeight,
                Depth = 24,
                Format = RenderTextureFormat.RHalf,
                Filter = FilterMode.Point
            };
            _StaticDepth = TextureUtility.CreateRenderTexture(staticDepthDesc);
        }

        // Releases all allocated textures
        private void ReleaseTextures()
        {
            Graphics.SetRenderTarget(null);

            // Release depth textures
            TextureUtility.Release(ref _Depth);
            TextureUtility.Release(ref _PreviousDepth);
            TextureUtility.Release(ref _StaticDepth);

            // Release simulation matrices
            TextureUtility.Release(ref _Buffers[0]);
            TextureUtility.Release(ref _Buffers[1]);
        }

        private void RefreshMaterialsData()
        {
            var filter = _Buffers[1].filterMode;

            // Bind the simulation matrix for screen-space rendering
            _Buffers[1].filterMode = FilterMode.Bilinear;
            _DisplacementMaterial.SetTexture("_SimulationMatrix", _Buffers[1]);

            _Buffers[1].filterMode = filter;
        }

        private void RefreshFade()
        {
            if (_DisplacementMaterial == null)
            {
                return;
            }
            _DisplacementMaterial.SetFloat("_Fadeout", _Fade ? 1.0f : 0.0f);
            _DisplacementMaterial.SetFloat("_FadePower", _FadePower);
        }

#if UNITY_EDITOR

        [MenuItem("GameObject/Water/SimulationArea", false, 10)]
        private static void CreateCustomGameObject(MenuCommand menuCommand)
        {
            var obj = new GameObject("Simulation Area");
            obj.AddComponent<MeshFilter>();
            obj.AddComponent<WaterSimulationArea>();

            GameObjectUtility.SetParentAndAlign(obj, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(obj, "Create " + obj.name);
            Selection.activeObject = obj;
        }

#endif

        #endregion Private Methods

        #region Helper Methods

        // Matches camera aspect and size with resolution
        private void MatchCameraResolution(ref Camera camera)
        {
            // Validate new resolution
            if (_Width <= 0 || _Height <= 0) return;

            // Calculate aspect ration and size
            var aspect = Resolution.x / Resolution.y;
            var halfSize = Size.y * 0.5f;

            // Assign values to camera
            camera.aspect = aspect;
            camera.orthographicSize = halfSize;
        }

        public void RenderLocalDisplacement(CommandBuffer commandBuffer, DynamicWaterCameraData overlays)
        {
            commandBuffer.DrawMesh(_MeshFilter.sharedMesh, transform.localToWorldMatrix, _DisplacementMaterial);
        }

        public void Enable()
        {
        }

        public void Disable()
        {
        }

        #endregion Helper Methods
    }
}