﻿namespace UltimateWater.Internal
{
    using System.Collections.Generic;
    using UnityEngine;

    public class DynamicWaterCameraData
    {
        #region Public Variables
        public RenderTexture DynamicDisplacementMap { get { return _Textures[(int)TextureTypes.Displacement]; } }
        public RenderTexture NormalMap { get { return _Textures[(int)TextureTypes.Normal]; } }
        public RenderTexture FoamMap { get { return _Textures[(int)TextureTypes.Foam]; } }
        public RenderTexture FoamMapPrevious { get { return _Textures[(int)TextureTypes.FoamPrevious]; } }
        public RenderTexture DisplacementsMask { get { return _Textures[(int)TextureTypes.DisplacementMask]; } }
        public RenderTexture DiffuseMap { get { return _Textures[(int)TextureTypes.Diffuse]; } }

        public RenderTexture TotalDisplacementMap
        {
            get
            {
                var totalDisplacement = _Textures[(int)TextureTypes.TotalDisplacement];

                if (!_TotalDisplacementMapDirty)
                {
                    return totalDisplacement;
                }

                _DynamicWater.RenderTotalDisplacementMap(Camera, totalDisplacement);
                _TotalDisplacementMapDirty = false;

                return totalDisplacement;
            }
        }

        public WaterCamera Camera { get; private set; }
        #endregion Public Variables

        #region Public Methods
        public DynamicWaterCameraData(DynamicWater dynamicWater, WaterCamera camera, int antialiasing)
        {
            ResolveFormats();

            Camera = camera;

            _DynamicWater = dynamicWater;
            _Antialiasing = antialiasing;

            camera.RenderTargetResized += Camera_RenderTargetResized;
            CreateRenderTargets();
        }

        public RenderTexture GetDebugMap(bool createIfNotExists = false)
        {
            // check if debug texture was created
            if (!_Textures.ContainsKey((int)TextureTypes.Debug))
            {
                // if we do not want to create the texture
                if (!createIfNotExists)
                {
                    return null;
                }

                _Textures.Add((int)TextureTypes.Debug, TextureUtility.CreateRenderTexture(DynamicDisplacementMap));
                _Textures[(int)TextureTypes.Debug].name = "Dynamic Water Map: Debug";

                return _Textures[(int)TextureTypes.Debug];
            }

            return _Textures[(int)TextureTypes.Debug];
        }

        public void Dispose()
        {
            Camera.RenderTargetResized -= Camera_RenderTargetResized;
            DisposeTextures();
        }
        public void ClearOverlays()
        {
            ValidateRTs();

            // [note]: using foreach allocates memory
            var enumerator = _Textures.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var entry = enumerator.Current;

                Graphics.SetRenderTarget(entry.Value);
                GL.Clear(false, true, entry.Key == (int)TextureTypes.DisplacementMask ? Color.white : Color.clear);
            }
            enumerator.Dispose();

            Graphics.SetRenderTarget(null);

            _TotalDisplacementMapDirty = true;
        }
        public void ValidateRTs()
        {
            // [note]: using foreach allocates memory
            var enumerator = _Textures.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var entry = enumerator.Current;
                TextureUtility.Verify(entry.Value);
            }
            enumerator.Dispose();
        }

        public void SwapFoamMaps()
        {
            var t = _Textures[(int)TextureTypes.FoamPrevious];
            _Textures[(int)TextureTypes.FoamPrevious] = _Textures[(int)TextureTypes.Foam];
            _Textures[(int)TextureTypes.Foam] = t;
        }
        #endregion Public Methods

        #region Private Types
        private enum TextureTypes
        {
            Displacement,
            DisplacementMask,
            Normal,
            Foam,
            FoamPrevious,
            Diffuse,
            Debug,
            TotalDisplacement
        }
        #endregion Private Types

        #region Private Variables
        internal int LastFrameUsed;

        private bool _TotalDisplacementMapDirty;
        private readonly int _Antialiasing;
        private readonly DynamicWater _DynamicWater;

        private readonly Dictionary<int, RenderTexture> _Textures
            = new Dictionary<int, RenderTexture>();

        private RenderTextureFormat _DisplacementFormat;
        private RenderTextureFormat _NormalFormat;
        private RenderTextureFormat _FoamFormat;
        private RenderTextureFormat _DiffuseFormat;
        private RenderTextureFormat _TotalDisplacementFormat;
        #endregion Private Variables

        #region Private Methods
        private void DisposeTextures()
        {
            foreach (var entry in _Textures)
            {
                entry.Value.Destroy();
            }
            _Textures.Clear();
        }

        private void Camera_RenderTargetResized(WaterCamera camera)
        {
            CreateRenderTargets();
        }

        private void CreateRenderTargets()
        {
            DisposeTextures();

            int width = Mathf.RoundToInt(Camera.CameraComponent.pixelWidth);
            int height = Mathf.RoundToInt(Camera.CameraComponent.pixelHeight);

            var displacementDesc = new TextureUtility.RenderTextureDesc()
            {
                Width = width,
                Height = height,
                Antialiasing = _Antialiasing,
                Format = _DisplacementFormat
            };
            var normalDesc = new TextureUtility.RenderTextureDesc()
            {
                Width = width,
                Height = height,
                Antialiasing = _Antialiasing,
                Format = _NormalFormat
            };
            var foamDesc = new TextureUtility.RenderTextureDesc()
            {
                Width = width,
                Height = height,
                Antialiasing = _Antialiasing,
                Format = _FoamFormat
            };
            var diffuseDesc = new TextureUtility.RenderTextureDesc()
            {
                Width = width,
                Height = height,
                Antialiasing = _Antialiasing,
                Format = _DiffuseFormat
            };
            var totalDisplacementDesc = new TextureUtility.RenderTextureDesc()
            {
                Width = 256,
                Height = 256,
                Antialiasing = 1,
                Format = _TotalDisplacementFormat
            };

            _Textures.Add((int)TextureTypes.Displacement, TextureUtility.CreateRenderTexture(displacementDesc));
            _Textures.Add((int)TextureTypes.DisplacementMask, TextureUtility.CreateRenderTexture(displacementDesc));
            _Textures.Add((int)TextureTypes.Normal, TextureUtility.CreateRenderTexture(normalDesc));
            _Textures.Add((int)TextureTypes.Foam, TextureUtility.CreateRenderTexture(foamDesc));
            _Textures.Add((int)TextureTypes.FoamPrevious, TextureUtility.CreateRenderTexture(foamDesc));
            _Textures.Add((int)TextureTypes.Diffuse, TextureUtility.CreateRenderTexture(diffuseDesc));

            _Textures.Add((int)TextureTypes.TotalDisplacement, TextureUtility.CreateRenderTexture(totalDisplacementDesc));
        }

        private void ResolveFormats()
        {
            bool result = true;
            // displacement && total displacement
            var format = Compatibility.GetFormat(RenderTextureFormat.ARGBHalf, new[] {
                RenderTextureFormat.ARGBFloat
            });
            result &= SetFormat(format, ref _DisplacementFormat);
            result &= SetFormat(format, ref _TotalDisplacementFormat);

            // normal && foam
            format = Compatibility.GetFormat(RenderTextureFormat.RGHalf, new[] {
                RenderTextureFormat.RGFloat
            });
            result &= SetFormat(format, ref _NormalFormat);
            result &= SetFormat(format, ref _FoamFormat);

            // diffuse
            format = Compatibility.GetFormat(RenderTextureFormat.ARGB32, new[] {
                RenderTextureFormat.ARGBHalf,
                RenderTextureFormat.ARGBFloat
            });
            result &= SetFormat(format,
                ref _DiffuseFormat);
        }

        private bool SetFormat(RenderTextureFormat? src, ref RenderTextureFormat dest)
        {
            if (!src.HasValue)
            {
                Debug.LogError("Target device does not support DynamicWaterEffects texture formats");
                return false;
            };

            dest = src.Value;
            return true;
        }
        #endregion Private Methods
    }
}