﻿using UltimateWater.Internal;

namespace UltimateWater
{
    using UnityEngine.Rendering;
    using UnityEngine;

    [RequireComponent(typeof(Renderer))]
    [AddComponentMenu("Ultimate Water/Dynamic/Water Interactive")]
    public class WaterInteractive : MonoBehaviour, IWavesInteractive
    {
        #region Public Variables
        [Tooltip("How much velocity modifies wave amplitude")]
        public float Multiplier = 1.0f;
        #endregion Public Variables

        #region Private Variables
        private Matrix4x4 _Previous;
        private Renderer _Renderer;

        private Material _Material;

        private bool _Enabled;
        #endregion Private Variables

        #region Unity Messages
        private void Awake()
        {
            _Material = ShaderUtility.Instance.CreateMaterial(ShaderList.Velocity);
            if (_Material.IsNotAssigned(this)) return;

            _Renderer = GetComponent<Renderer>();
            if (_Renderer.IsNotAssigned(this)) return;

            _Enabled = _Renderer.enabled;

            _Previous = transform.localToWorldMatrix;
        }

        private void FixedUpdate()
        {
            var current = transform.localToWorldMatrix;

            _Material.SetMatrix("_PreviousWorld", _Previous);
            _Material.SetMatrix("_CurrentWorld", current);
            _Material.SetFloat("_Data", Multiplier / Time.fixedDeltaTime);

            _Previous = current;
        }

        private void OnEnable()
        {
            DynamicWater.AddRenderer(this);
        }
        private void OnDisable()
        {
            DynamicWater.RemoveRenderer(this);
        }
        #endregion Unity Messages

        public void Enable()
        {
            _Renderer.enabled = true;
        }
        public void Disable()
        {
            _Renderer.enabled = _Enabled;
        }

        public void Render(CommandBuffer commandBuffer)
        {
            commandBuffer.DrawRenderer(_Renderer, _Material);
        }
    }
}