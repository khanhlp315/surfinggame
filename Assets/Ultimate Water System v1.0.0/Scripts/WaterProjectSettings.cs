using UnityEngine;

namespace UltimateWater
{
    public class WaterProjectSettings : ScriptableObjectSingleton
    {
        #region Inspector Variables
#pragma warning disable 0414
        [SerializeField]
        private float serializedVersion = 1.0f;
#pragma warning restore 0414

        [SerializeField]
        private int waterLayer = 4;

        [Tooltip("Used for some camera effects. Has to be unused. You don't need to mask it on your cameras.")]
        [SerializeField]
        private int waterTempLayer = 22;

        [Tooltip("UltimateWater internally uses colliders to detect camera entering into subtractive volumes etc. You will have to ignore this layer in your scripting raycasts.")]
        [SerializeField]
        private int waterCollidersLayer = 1;

        [Tooltip("Each scene with water needs one unique asset file somewhere in your project. By default these files are generated automatically, but you may choose to create them manually.")]
        [SerializeField]
        private WaterAssetFilesCreation assetFilesCreation;

        [Tooltip("More threads increase physics precision under stress, but also decrease overall performance a bit.")]
        [SerializeField]
        private int physicsThreads = 1;

        [SerializeField]
        private System.Threading.ThreadPriority physicsThreadsPriority = System.Threading.ThreadPriority.BelowNormal;

        [SerializeField]
        private bool allowCpuFFT = true;

        [Tooltip("Some hardware doesn't support floating point mip maps correctly and they are forcefully disabled. You may simulate how the water would look like on such hardware by disabling this option. Most notably fp mip maps don't work correctly on most AMD graphic cards (for now).")]
        [SerializeField]
        private bool allowFloatingPointMipMaps = true;

        [SerializeField]
        private bool debugPhysics;

        [SerializeField]
        private bool askForWaterCameras = true;

        [SerializeField]
        private AbsorptionEditMode absorptionEditMode = AbsorptionEditMode.Transmission;

        [SerializeField]
        private SpecularEditMode specularEditMode = SpecularEditMode.IndexOfRefraction;

        [SerializeField] private bool _SinglePassStereoRendering;
        [SerializeField] private bool _DisableMultisampling = true;
        #endregion Inspector Variables

        #region Public Variables
        public static WaterProjectSettings Instance
        {
            get
            {
                if (_NoInstance)         // performance
                {
                    _Instance = LoadSingleton<WaterProjectSettings>();
                    _NoInstance = false;
                }

                return _Instance;
            }
        }

        public int PhysicsThreads
        {
            get { return physicsThreads; }
            set { physicsThreads = value; }
        }

        public int WaterLayer
        {
            get { return waterLayer; }
        }

        public int WaterTempLayer
        {
            get { return waterTempLayer; }
        }

        public int WaterCollidersLayer
        {
            get { return waterCollidersLayer; }
        }

        public WaterAssetFilesCreation AssetFilesCreation
        {
            get { return assetFilesCreation; }
        }

        public System.Threading.ThreadPriority PhysicsThreadsPriority
        {
            get { return physicsThreadsPriority; }
        }

        public bool AllowCpuFFT
        {
            get { return allowCpuFFT; }
        }

        public bool AllowFloatingPointMipMaps
        {
            get
            {
                string vendor = SystemInfo.graphicsDeviceVendor.ToLower();
                return !vendor.Contains("amd") && !vendor.Contains("ati") && !SystemInfo.graphicsDeviceName.ToLower().Contains("radeon") && allowFloatingPointMipMaps;
            }
        }

        public bool DebugPhysics
        {
            get { return debugPhysics; }
        }

        public bool AskForWaterCameras
        {
            get { return askForWaterCameras; }
            set { askForWaterCameras = value; }
        }

        public AbsorptionEditMode InspectorAbsorptionEditMode
        {
            get { return absorptionEditMode; }
        }

        public SpecularEditMode InspectorSpecularEditMode
        {
            get { return specularEditMode; }
        }

        public bool SinglePassStereoRendering
        {
            get { return _SinglePassStereoRendering; }
        }

        public static readonly float CurrentVersion = 1.0f;
        public static readonly string CurrentVersionString = "1.0.0";
        #endregion Public Variables

        #region Public Methods
#if UNITY_EDITOR
        public void OnValidate()
        {
#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
            bool singlePassStereoRendering = false;
#elif UNITY_5_4
            bool singlePassStereoRendering = UnityEditor.PlayerSettings.virtualRealitySupported && UnityEditor.PlayerSettings.singlePassStereoRendering;
#else
            bool singlePassStereoRendering = UnityEditor.PlayerSettings.virtualRealitySupported && UnityEditor.PlayerSettings.stereoRenderingPath != UnityEditor.StereoRenderingPath.MultiPass;
#endif

            if (_SinglePassStereoRendering != singlePassStereoRendering)
            {
                _SinglePassStereoRendering = singlePassStereoRendering;
                UnityEditor.EditorUtility.SetDirty(this);
            }

            if (_DisableMultisampling)
            {
                QualitySettings.antiAliasing = 1;
            }
        }
#endif
        #endregion Public Methods

        #region Public Types
        public enum WaterAssetFilesCreation
        {
            Automatic,
            Manual
        }

        public enum AbsorptionEditMode
        {
            Absorption,
            Transmission
        }

        public enum SpecularEditMode
        {
            IndexOfRefraction,
            CustomColor
        }
        #endregion Public Types

        #region Private Variables
        private static WaterProjectSettings _Instance;
        private static bool _NoInstance = true;
        #endregion Private Variables
    }
}
