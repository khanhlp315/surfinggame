﻿namespace UltimateWater
{
    using System.Collections.Generic;
    using UnityEngine;
    using Internal;

    public class PlanarReflection : IWaterModule
    {
        #region Public Types
        [System.Serializable]
        public class Data
        {
            public LayerMask ReflectionMask = int.MaxValue;
            public bool ReflectSkybox = true;
            public bool RenderShadows = true;

            [Range(0.0f, 1.0f)]
            public float Resolution = 0.5f;

            [Range(0.0f, 1.0f)]
            [Tooltip("Allows you to use more rational resolution of planar reflections on screens with very high dpi. Planar reflections should be blurred anyway.")]
            public float RetinaResolution = 0.333f;
        }
        #endregion Public Types

        #region Public Variables
        public float Resolution
        {
            get { return data.Resolution; }
            set
            {
                data.Resolution = value;
                CalculateResolutionMultiplier();
            }
        }
        public float RetinaResolution
        {
            get { return data.RetinaResolution; }
            set
            {
                data.RetinaResolution = value;
                CalculateResolutionMultiplier();
            }
        }

        public bool ReflectSkybox
        {
            get { return data.ReflectSkybox; }
            set { data.ReflectSkybox = value; }
        }
        public bool RenderShadows
        {
            get { return data.RenderShadows; }
            set { data.RenderShadows = value; }
        }

        public LayerMask ReflectionMask
        {
            get { return data.ReflectionMask; }
            set { data.ReflectionMask = value; }
        }
        #endregion Public Variables

        #region Private Variables
        private readonly Data data;
        private readonly Water water;
        private readonly bool systemSupportsHDR;
        private readonly Dictionary<Camera, TemporaryRenderTexture> temporaryTargets =
            new Dictionary<Camera, TemporaryRenderTexture>();

        private TemporaryRenderTexture currentTarget;
        private float finalResolutionMultiplier;
        private bool renderPlanarReflections;
        private Material utilitiesMaterial;
        private Shader utilitiesShader;

        private static int reflectionTexProperty;

        private const float ClipPlaneOffset = 0.07f;
        #endregion Private Variables

        #region Public Methods
        public PlanarReflection(Water water, Data data)
        {
            this.water = water;
            this.data = data;

            reflectionTexProperty = Shader.PropertyToID("_PlanarReflectionTex");
            systemSupportsHDR = SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf);

            Validate();

            water.ProfilesManager.Changed.AddListener(OnProfilesChanged);
            OnProfilesChanged(water);
        }
        internal override void OnWaterPostRender(WaterCamera waterCamera)
        {
            var camera = waterCamera.CameraComponent;

            TemporaryRenderTexture renderTexture;

            if (temporaryTargets.TryGetValue(camera, out renderTexture))
            {
                temporaryTargets.Remove(camera);
                renderTexture.Dispose();
            }
        }
        #endregion Public Methods

        #region Private Methods

        internal override void Start(Water water)
        {
        }
        internal override void Enable()
        {
        }
        internal override void Disable()
        {
        }

        internal override void Validate()
        {
            if (utilitiesShader == null)
                utilitiesShader = Shader.Find("UltimateWater/Utilities/PlanarReflection - Utilities");

            data.Resolution = Mathf.Clamp01(Mathf.RoundToInt(data.Resolution * 10.0f) * 0.1f);
            data.RetinaResolution = Mathf.Clamp01(Mathf.RoundToInt(data.RetinaResolution * 10.0f) * 0.1f);

            CalculateResolutionMultiplier();
        }

        internal override void Destroy()
        {
            ClearRenderTextures();
        }

        internal override void Update()
        {
            ClearRenderTextures();
        }

        internal override void OnWaterRender(WaterCamera waterCamera)
        {
            var camera = waterCamera.CameraComponent;

            if (!camera.enabled || !renderPlanarReflections)
                return;

            if (!temporaryTargets.TryGetValue(camera, out currentTarget))
            {
                var reflectionCamera = Reflection.GetReflectionCamera(camera);

                RenderReflection(camera, reflectionCamera);
                UpdateRenderProperties(reflectionCamera);
            }
        }

        private void CalculateResolutionMultiplier()
        {
            float final = Screen.dpi <= 220 ? data.Resolution : data.RetinaResolution;

            if (this.finalResolutionMultiplier != final)
            {
                this.finalResolutionMultiplier = final;
                ClearRenderTextures();
            }
        }

        private void RenderReflection(Camera camera, Camera reflectionCamera)
        {
            reflectionCamera.cullingMask = data.ReflectionMask;

            SetCameraSettings(camera, reflectionCamera);

            currentTarget = GetRenderTexture(camera.pixelWidth, camera.pixelHeight, reflectionCamera);
            temporaryTargets[camera] = currentTarget;

            var target = RenderTexturesCache.GetTemporary(currentTarget.Texture.width, currentTarget.Texture.height, 16, currentTarget.Texture.format, true, false);
            reflectionCamera.targetTexture = target;

            reflectionCamera.transform.eulerAngles = CalculateReflectionAngles(camera);
            reflectionCamera.transform.position = CalculateReflectionPosition(camera);

            float d = -water.transform.position.y - ClipPlaneOffset;
            Vector4 reflectionPlane = new Vector4(0, 1, 0, d);

            Matrix4x4 reflection = Matrix4x4.zero;
            reflection = Reflection.CalculateReflectionMatrix(reflection, reflectionPlane);
            Vector3 newpos = reflection.MultiplyPoint(camera.transform.position);

            reflectionCamera.worldToCameraMatrix = camera.worldToCameraMatrix * reflection;

            Vector4 clipPlane = Reflection.CameraSpacePlane(reflectionCamera, water.transform.position, new Vector3(0, 1, 0), ClipPlaneOffset, 1.0f);

            var matrix = camera.projectionMatrix;
            matrix = Reflection.CalculateObliqueMatrix(matrix, clipPlane);
            reflectionCamera.projectionMatrix = matrix;

            reflectionCamera.transform.position = newpos;
            Vector3 angles = camera.transform.eulerAngles;
            reflectionCamera.transform.eulerAngles = new Vector3(-angles.x, angles.y, angles.z);

#if SKYBOX_CLEAR_BUG_FIX
            Graphics.SetRenderTarget(target);
            GL.Clear(true, true, new Color(0.0f, 0.0f, 0.0f, 0.0f));
#endif

            reflectionCamera.clearFlags = data.ReflectSkybox ? CameraClearFlags.Skybox : CameraClearFlags.SolidColor;

            if (data.RenderShadows)
            {
                GL.invertCulling = true;
                reflectionCamera.Render();
                GL.invertCulling = false;
            }
            else
            {
#if UNITY_5_5_OR_NEWER
                var originalShadowQuality = QualitySettings.shadows;
                QualitySettings.shadows = ShadowQuality.Disable;
# else
                var shadowResolution = QualitySettings.shadowResolution;
                QualitySettings.shadowResolution = 0;
#endif

                GL.invertCulling = true;
                reflectionCamera.Render();
                GL.invertCulling = false;

#if UNITY_5_5_OR_NEWER
                QualitySettings.shadows = originalShadowQuality;
#else
                QualitySettings.shadowResolution = shadowResolution;
#endif
            }

            reflectionCamera.targetTexture = null;

            if (utilitiesMaterial == null)
                utilitiesMaterial = new Material(utilitiesShader) { hideFlags = HideFlags.DontSave };

            Graphics.Blit(target, currentTarget, utilitiesMaterial, 0);
            target.Dispose();
        }

        private void UpdateRenderProperties(Camera reflectionCamera)
        {
            var block = water.Renderer.PropertyBlock;
            block.SetTexture(reflectionTexProperty, currentTarget);
            block.SetMatrix("_PlanarReflectionProj",
                (Matrix4x4.TRS(new Vector3(0.5f, 0.5f, 0.0f), Quaternion.identity, new Vector3(0.5f, 0.5f, 1.0f)) *
                 reflectionCamera.projectionMatrix * reflectionCamera.worldToCameraMatrix));
            block.SetFloat("_PlanarReflectionMipBias", -Mathf.Log(1.0f / finalResolutionMultiplier, 2));
        }

        private TemporaryRenderTexture GetRenderTexture(int width, int height, Camera reflectionCamera)
        {
            int adaptedWidth = Mathf.ClosestPowerOfTwo(Mathf.RoundToInt(width * finalResolutionMultiplier));
            int adaptedHeight = Mathf.ClosestPowerOfTwo(Mathf.RoundToInt(height * finalResolutionMultiplier));
#if UNITY_5_6_OR_NEWER
            bool hdr = reflectionCamera.allowHDR;
#else
            bool hdr = reflectionCamera.hdr;
#endif

            var renderTexture = RenderTexturesCache.GetTemporary(adaptedWidth, adaptedHeight, 0,
                hdr && systemSupportsHDR ? RenderTextureFormat.ARGBHalf : RenderTextureFormat.ARGB32, true, false, true);

            renderTexture.Texture.filterMode = FilterMode.Trilinear;
            renderTexture.Texture.wrapMode = TextureWrapMode.Clamp;

            return renderTexture;
        }

        private void ClearRenderTextures()
        {
            var enumerator = temporaryTargets.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var entry = enumerator.Current;
                entry.Value.Dispose();
            }
            enumerator.Dispose();

            temporaryTargets.Clear();
        }

        private void OnProfilesChanged(Water water)
        {
            var profiles = water.ProfilesManager.Profiles;

            if (profiles == null)
                return;

            float intensity = 0.0f;

            for (int i = profiles.Length - 1; i >= 0; --i)
            {
                var weightedProfile = profiles[i];

                var profile = weightedProfile.Profile;
                float weight = weightedProfile.Weight;

                intensity += profile.PlanarReflectionIntensity * weight;
            }

            renderPlanarReflections = intensity > 0.0f;
        }
        #endregion Private Methods

        #region Helper Methods
        private void SetCameraSettings(Camera source, Camera destination)
        {
            destination.backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
            destination.fieldOfView = source.fieldOfView;
            destination.aspect = source.aspect;

#if UNITY_5_6_OR_NEWER
            destination.allowHDR = systemSupportsHDR && source.allowHDR;
#else
            destination.hdr = systemSupportsHDR && source.hdr;
#endif
        }

        private Vector3 CalculateReflectionPosition(Camera camera)
        {
            Vector3 position = camera.transform.position;
            position.y = water.transform.position.y - position.y;
            return position;
        }
        private Vector3 CalculateReflectionAngles(Camera camera)
        {
            Vector3 angles = camera.transform.eulerAngles;
            return new Vector3(-angles.x, angles.y, angles.z);
        }
        #endregion Helper Methods
    }
}