﻿namespace UltimateWater
{
    using Internal;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.Rendering;

    /// <summary>
    ///     Each camera supposed to see water needs this component attached. Renders all camera-specific maps for the water:
    ///     <list type="bullet">
    ///         <item>Depth Maps</item>
    ///         <item>Displaced water info map</item>
    ///         <item>Volume maps</item>
    ///     </list>
    /// </summary>
#if UNITY_5_4_OR_NEWER
    [ImageEffectAllowedInSceneView]
#endif
    [AddComponentMenu("Ultimate Water/Water Camera", -1)]
    [ExecuteInEditMode]
    public sealed class WaterCamera : MonoBehaviour
    {
        #region Public Types
        public enum CameraType
        {
            /// <summary>
            /// It's a camera that has attached WaterCamera component.
            /// </summary>
            Normal,

            /// <summary>
            /// It's a camera used for depth rendering etc.
            /// </summary>
            Effect,

            /// <summary>
            /// It's a camera used for rendering water before displaying it in image effect render modes.
            /// </summary>
            RenderHelper
        }

        [System.Serializable]
        public class WaterCameraEvent : UnityEvent<WaterCamera> { }
        #endregion Public Types

        #region Inspector Variables
        [SerializeField]
        private WaterRenderMode renderMode;

        [SerializeField]
        private WaterGeometryType geometryType = WaterGeometryType.Auto;

        [SerializeField]
        private bool renderWaterDepth = true;

        [Tooltip("Water has a pretty smooth shape so it's often safe to render it's depth in a lower resolution than the rest of the scene." +
                 " Although the default value is 1.0, you may probably safely use 0.5 and gain some minor performance boost. " +
                 "If you will encounter any artifacts in masking or image effects, set it back to 1.0.")]
        [Range(0.2f, 1.0f)]
        [SerializeField]
        private float baseEffectsQuality = 1.0f;

        [SerializeField]
        private float superSampling = 1.0f;

        [SerializeField]
        private bool renderVolumes = true;

        [SerializeField]
        private bool renderFlatMasks = true;

        [SerializeField]
        private int forcedVertexCount = 0;

        [SerializeField]
        private WaterCameraEvent submersionStateChanged;

        [Tooltip("Optional. Deferred rendering mode will try to match profile parameters of this water object as well as possible. " +
                 "It affects only some minor parameters and you may generally ignore this setting. May be removed in the future.")]
        [SerializeField]
        private Water mainWater;

        [SerializeField]
        private LightWaterEffects effectsLight;

        //[Tooltip("Determines how much extra padding should be added for screen-space buffers to ensure that the waves" +
        //         " travelling inside the view frustum are rendered correctly. Higher values mean lower quality. " +
        //         "In VR it's recommended to set this to 0 (or very low) as there is enough padding already and flickering may appear otherwise.")]
        //[Range(0.0f, 1.0f)]
        //[SerializeField]
        //private float screenSpaceMapsBorder = 0.25f;
        #endregion Inspector Variables

        #region Public Variables
        public Water MainWater
        {
            get
            {
                if (mainWater != null)
                {
                    return mainWater;
                }

                var boundlessWaters = WaterGlobals.Instance.BoundlessWaters;
                if (boundlessWaters.Count != 0)
                    return boundlessWaters[0];

                var waters = customWaterRenderList ?? WaterGlobals.Instance.Waters;

                if (waters.Count != 0)
                    return waters[0];

                return null;
            }
        }

        public bool RenderWaterDepth
        {
            get { return renderWaterDepth; }
            set { renderWaterDepth = value; }
        }

        public bool RenderVolumes
        {
            get { return renderVolumes; }
            set { renderVolumes = value; }
        }

        public float BaseEffectsQuality
        {
            get { return baseEffectsQuality; }
        }

        public CameraType Type
        {
            get { return waterCameraType; }
            set { waterCameraType = value; }
        }

        public WaterGeometryType GeometryType
        {
            get { return geometryType; }
            set { geometryType = value; }
        }

        public Rect LocalMapsRect
        {
            get { return localMapsRect; }
        }

        public WaterRenderMode RenderMode
        {
            get { return renderMode; }
            set
            {
                renderMode = value;
                OnDisable();
                OnEnable();
            }
        }

        public Rect LocalMapsRectPrevious
        {
            get { return localMapsRectPrevious; }
        }

        public Vector4 LocalMapsShaderCoords
        {
            get
            {
                float invWidth = 1.0f / localMapsRect.width;
                return new Vector4(-localMapsRect.xMin * invWidth, -localMapsRect.yMin * invWidth, invWidth, localMapsRect.width);
            }
        }

        public int ForcedVertexCount
        {
            get { return forcedVertexCount; }
            set { forcedVertexCount = value; }
        }

        public Water ContainingWater
        {
            get { return baseCamera == null ? (submersionState != SubmersionState.None ? containingWater : null) : baseCamera.ContainingWater; }
        }

        public float WaterLevel
        {
            get { return waterLevel; }
        }

        public SubmersionState SubmersionState
        {
            get { return submersionState; }
        }

        public Camera CameraComponent
        {
            get { return cameraComponent; }
        }

        [HideInInspector]
        public Camera ReflectionCamera;

        public bool RenderFlatMasks
        {
            get { return renderFlatMasks; }
        }

        public static List<WaterCamera> EnabledWaterCameras
        {
            get { return enabledWaterCameras; }
        }

        /// <summary>
        /// Ready to render alternative camera for effects.
        /// </summary>
        public Camera EffectsCamera
        {
            get
            {
                if (waterCameraType == CameraType.Normal && effectCamera == null)
                    effectCamera = CreateEffectsCamera(CameraType.Effect);

                return effectCamera;
            }
        }

        public Camera PlaneProjectorCamera
        {
            get
            {
                if (waterCameraType == CameraType.Normal && planeProjectorCamera == null)
                    planeProjectorCamera = CreateEffectsCamera(CameraType.Effect);

                return planeProjectorCamera;
            }
        }

        public WaterCameraEvent SubmersionStateChanged
        {
            get { return submersionStateChanged ?? (submersionStateChanged = new WaterCameraEvent()); }
        }

        public int BaseEffectWidth
        {
            get { return pixelWidth; }
        }
        public int BaseEffectHeight
        {
            get { return pixelWidth; }
        }

        public Camera WaterRenderCamera
        {
            get
            {
                if (waterRenderCamera == null)
                {
                    waterRenderCamera = CreateEffectsCamera(CameraType.RenderHelper);
                }
                return waterRenderCamera;
            }
        }

        public bool IsInsideAdditiveVolume
        {
            get { return isInsideAdditiveVolume; }
        }
        public bool IsInsideSubtractiveVolume
        {
            get { return isInsideSubtractiveVolume; }
        }

        public LightWaterEffects EffectsLight
        {
            get { return effectsLight; }
            set { effectsLight = value; }
        }

        public float SuperSampling
        {
            get { return superSampling; }
        }

        public event System.Action<WaterCamera> RenderTargetResized;

        public event System.Action<WaterCamera> Destroyed;

        public event System.Action<WaterCamera> Disabled;

        public static event System.Action<WaterCamera> OnGlobalPreCull;

        public static event System.Action<WaterCamera> OnGlobalPostRender;

        public static bool RenderInSceneView = true;
        #endregion Public Variables

        #region Private Variables

        private readonly IRenderModule _DepthModule = new DepthModule();
        private readonly IRenderModule _MaskModule = new MaskModule();

        private readonly IRenderModule _ForwardModule = new ForwardModule();
        private readonly IRenderModule _DeferredModule = new DeferredModule();

        private WaterCamera baseCamera;
        private Camera effectCamera;
        private Camera mainCamera;
        private Camera planeProjectorCamera;

        private Camera cameraComponent;
        private Camera waterRenderCamera;

        private WaterCameraIME waterCameraIME;

        private Water containingWater;
        private WaterSample waterSample;
        private List<Water> customWaterRenderList;

        private bool effectsEnabled;
        private IWaterImageEffect[] imageEffects;
        private CameraType waterCameraType;

        private SubmersionState submersionState;
        private bool isInsideSubtractiveVolume;
        private bool isInsideAdditiveVolume;

        private Rect localMapsRect;
        private Rect localMapsRectPrevious;
        private Rect shadowedWaterRect;
        private int pixelWidth, pixelHeight;
        private Mesh shadowsEnforcerMesh;
        private Material shadowsEnforcerMaterial;

        private float waterLevel;

        private Matrix4x4 lastPlaneProjectorMatrix;

        private CommandBuffer imageEffectCommands;
        private RenderTexture depthTex;

        private CommandBuffer _UtilityCommandBuffer;

        private static readonly Dictionary<Camera, WaterCamera> waterCamerasCache = new Dictionary<Camera, WaterCamera>();
        private static readonly List<WaterCamera> enabledWaterCameras = new List<WaterCamera>();
        #endregion Private Variables

        #region Unity Messages
        private void Awake()
        {
            Shader.SetGlobalFloat("_Invert", 0.0f);

            OnValidate();
            cameraComponent = GetComponent<Camera>();
        }

        private void OnEnable()
        {
            _UtilityCommandBuffer = new CommandBuffer { name = "[Ultimate Water]: Utility Command Buffer" };

            waterCamerasCache[cameraComponent] = this;

            if (waterCameraType == CameraType.Normal)
            {
                enabledWaterCameras.Add(this);
                imageEffects = GetComponents<IWaterImageEffect>();

                foreach (var imageEffect in imageEffects)
                {
                    imageEffect.OnWaterCameraEnabled();
                }
            }

            RemoveUtilityCommands();
            AddUtilityCommands();

            _DepthModule.OnEnable(this);
            _MaskModule.OnEnable(this);

            _ForwardModule.OnEnable(this);
            _DeferredModule.OnEnable(this);
        }
        private void OnDisable()
        {
            _MaskModule.OnDisable(this);
            _DepthModule.OnDisable(this);

            _ForwardModule.OnDisable(this);
            _DeferredModule.OnDisable(this);

            if (waterCameraType == CameraType.Normal)
                enabledWaterCameras.Remove(this);

            ReleaseImageEffectTemporaryTextures();

            RemoveUtilityCommands();
            DisableEffects();

            if (effectCamera != null)
            {
                effectCamera.gameObject.Destroy();
                effectCamera = null;
            }

            if (planeProjectorCamera != null)
            {
                planeProjectorCamera.gameObject.Destroy();
                planeProjectorCamera = null;
            }

            if (waterSample != null)
            {
                waterSample.Stop();
                waterSample = null;
            }

            containingWater = null;

            _UtilityCommandBuffer = null;

            if (Disabled != null)
                Disabled(this);
        }
        private void OnDestroy()
        {
            waterCamerasCache.Remove(GetComponent<Camera>());

            if (Destroyed != null)
            {
                Destroyed(this);
                Destroyed = null;
            }
        }

        private void Update()
        {
            switch (renderMode)
            {
                case WaterRenderMode.ImageEffectDeferred:
                {
                    ReleaseImageEffectTemporaryTextures();
                    break;
                }
            }
        }

        private void OnPreCull()
        {
            if (!Application.isPlaying || !RenderInSceneView && WaterUtilities.IsSceneViewCamera(cameraComponent) || !enabled)
            {
                return;
            }

            if (WaterUtilities.IsSceneViewCamera(cameraComponent))
            {
                var camera = cameraComponent.GetComponent<WaterCamera>();
                camera.renderMode = WaterRenderMode.DefaultQueue;
            }

            if (OnGlobalPreCull != null)
                OnGlobalPreCull(this);

            if (waterCameraType == CameraType.RenderHelper)
            {
                var camera = mainCamera.GetComponent<WaterCamera>();
                if (camera != null)
                {
                    camera.RenderWaterDirect();
                }
                return;
            }

            bool hasEffectsLight = effectsLight != null;

            if (waterCameraType == CameraType.Normal)
            {
                bool flip = WaterUtilities.IsSceneViewCamera(cameraComponent) ||
                            (VersionCompatibility.Version >= 560 && renderMode == WaterRenderMode.DefaultQueue &&
#if UNITY_5_6_OR_NEWER
                                 cameraComponent.allowHDR
#else
                                 cameraComponent.hdr
#endif
                            );

                SetPlaneProjectorMatrix(renderMode != WaterRenderMode.DefaultQueue, flip);

                ToggleEffects();

                PrepareToRender();
                SetFallbackTextures();
            }

            if (effectsEnabled)
            {
                SetLocalMapCoordinates();
            }

            RenderWaterEffects();

            if (hasEffectsLight)
            {
                effectsLight.PrepareRenderingOnCamera(this);
            }

            if (renderMode == WaterRenderMode.DefaultQueue)
            {
                RenderWaterDirect();
            }

            if (!effectsEnabled)
            {
                Shader.DisableKeyword("WATER_BUFFERS_ENABLED");
                return;
            }
            Shader.EnableKeyword("WATER_BUFFERS_ENABLED");

            _MaskModule.Process(this);
            _DepthModule.Process(this);

            if (imageEffects != null && Application.isPlaying)
            {
                for (int i = 0; i < imageEffects.Length; ++i)
                {
                    imageEffects[i].OnWaterCameraPreCull();
                }
            }

            if (shadowedWaterRect.xMin < shadowedWaterRect.xMax)
                RenderShadowEnforcers();

            if (renderMode != WaterRenderMode.DefaultQueue)
            {
                WaterMaterials.ValidateGlobalWaterDataLookupTex();
            }
        }
        private void OnPostRender()
        {
            if (!Application.isPlaying || !RenderInSceneView && WaterUtilities.IsSceneViewCamera(cameraComponent))
            {
                return;
            }

            var waters = customWaterRenderList ?? WaterGlobals.Instance.Waters;

            for (int waterIndex = waters.Count - 1; waterIndex >= 0; --waterIndex)
                waters[waterIndex].Renderer.PostRender(this);

            if ((object)effectsLight != null)
                effectsLight.CleanRenderingOnCamera();

            if (OnGlobalPostRender != null)
                OnGlobalPostRender(this);
        }

        private void OnValidate()
        {
            _MaskModule.OnValidate(this);
            _DepthModule.OnValidate(this);

            _ForwardModule.OnValidate(this);
            _DeferredModule.OnValidate(this);

            ShaderUtility.Instance.Use(ShaderList.ShadowEnforcer);

            if (waterCameraIME == null)
                waterCameraIME = GetComponent<WaterCameraIME>() ?? gameObject.AddComponent<WaterCameraIME>();

            if (renderMode == WaterRenderMode.ImageEffectDeferred)
            {
                renderWaterDepth = true;
                baseEffectsQuality = 1.0f;
            }

            cameraComponent = GetComponent<Camera>();

#if UNITY_EDITOR
            ReorderWaterCameraIME();
#endif

            RemoveUtilityCommands();

            if (enabled)
                AddUtilityCommands();

            waterCameraIME.enabled = enabled && (renderMode == WaterRenderMode.ImageEffectDeferred || renderMode == WaterRenderMode.ImageEffectForward);
        }

        private void Reset()
        {
            var camera = GetComponent<Camera>();
            if (camera == null)
            {
                Debug.LogError("Water Camera component should be attached to GameObject containing Camera component");

                var ime = GetComponent<WaterCameraIME>();
                if (ime != null)
                {
                    ime.Destroy();
                }

                this.Destroy();
            }
        }
        #endregion Unity Messages

        #region Public Methods
        /// <summary>
        /// Use this method to set a custom list of waters that should be rendered by this WaterCamera. Pass null to revert back to the default behaviour.
        /// </summary>
        /// <param name="waters"></param>
        public void SetCustomWaterRenderList(List<Water> waters)
        {
            customWaterRenderList = waters;
        }

        /// <summary>
        /// Fast and allocation free way to get a WaterCamera component attached to camera.
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="forceAdd"></param>
        /// <returns></returns>
        public static WaterCamera GetWaterCamera(Camera camera, bool forceAdd = false)
        {
            WaterCamera waterCamera;

            if (!waterCamerasCache.TryGetValue(camera, out waterCamera))
            {
                waterCamera = camera.GetComponent<WaterCamera>();

                if (waterCamera != null)
                    waterCamerasCache[camera] = waterCamera;
                else if (forceAdd)
                    waterCamerasCache[camera] = camera.gameObject.AddComponent<WaterCamera>();
                else
                {
                    // force null reference (Unity uses custom null checks)
                    // ReSharper disable once RedundantAssignment
                    waterCamerasCache[camera] = waterCamera = null;
                }
            }

            return waterCamera;
        }

        public void CopyFrom(WaterCamera waterCamera)
        {
            localMapsRect = waterCamera.localMapsRect;
            localMapsRectPrevious = waterCamera.localMapsRectPrevious;
            geometryType = waterCamera.geometryType;
        }

        public void RenderWaterWithShader(string commandName, RenderTexture target, Shader shader, Water water)
        {
            _UtilityCommandBuffer.Clear();
#if UNITY_EDITOR
            _UtilityCommandBuffer.name = commandName;
#endif
            _UtilityCommandBuffer.SetRenderTarget(target);

            water.Renderer.Render(cameraComponent, geometryType, _UtilityCommandBuffer, shader);

            GL.PushMatrix();
            GL.modelview = cameraComponent.worldToCameraMatrix;
            GL.LoadProjectionMatrix(cameraComponent.projectionMatrix);

            Graphics.ExecuteCommandBuffer(_UtilityCommandBuffer);

            GL.PopMatrix();
        }

        public void RenderWaterWithShader(string commandName, RenderTexture target, Shader shader, bool surfaces, bool volumes, bool volumesTwoPass)
        {
            _UtilityCommandBuffer.Clear();
#if UNITY_EDITOR
            _UtilityCommandBuffer.name = commandName;
#endif
            _UtilityCommandBuffer.SetRenderTarget(target);

            AddWaterRenderCommands(_UtilityCommandBuffer, shader, surfaces, volumes, volumesTwoPass);

            GL.PushMatrix();
            GL.modelview = cameraComponent.worldToCameraMatrix;
            GL.LoadProjectionMatrix(cameraComponent.projectionMatrix);

            Graphics.ExecuteCommandBuffer(_UtilityCommandBuffer);

            GL.PopMatrix();
        }
        #endregion Public Methods

        #region Private Methods

#if UNITY_EDITOR
        private void ReorderWaterCameraIME()
        {
            var components = GetComponents<Component>();

            int index = components.Length - 1;

            while (!(components[index] is WaterCameraIME))
            {
                if (components[index] is WaterCamera)
                {
                    while (!(components[index--] is WaterCameraIME))
                        UnityEditorInternal.ComponentUtility.MoveComponentDown(waterCameraIME);

                    return;
                }

                --index;
            }

            --index;

            while (!(components[index--] is WaterCamera))
                UnityEditorInternal.ComponentUtility.MoveComponentUp(waterCameraIME);
        }
#endif

        internal void ReportShadowedWaterMinMaxRect(Vector2 min, Vector2 max)
        {
            if (shadowedWaterRect.xMin > min.x)
                shadowedWaterRect.xMin = min.x;

            if (shadowedWaterRect.yMin > min.y)
                shadowedWaterRect.yMin = min.y;

            if (shadowedWaterRect.xMax < max.x)
                shadowedWaterRect.xMax = max.x;

            if (shadowedWaterRect.yMax < max.y)
                shadowedWaterRect.yMax = max.y;
        }

        private void RenderWaterDirect()
        {
            var waters = customWaterRenderList ?? WaterGlobals.Instance.Waters;
            for (int waterIndex = waters.Count - 1; waterIndex >= 0; --waterIndex)
            {
                waters[waterIndex].Renderer.Render(cameraComponent, geometryType);
            }
        }

        public void AddWaterRenderCommands(CommandBuffer commandBuffer, Shader shader, bool surfaces, bool volumes, bool volumesTwoPass)
        {
            var waters = customWaterRenderList ?? WaterGlobals.Instance.Waters;

            if (volumes)
            {
                for (int waterIndex = waters.Count - 1; waterIndex >= 0; --waterIndex)
                {
                    waters[waterIndex].Renderer.RenderVolumes(commandBuffer, shader, volumesTwoPass);
                }
            }

            if (surfaces)
            {
                for (int waterIndex = waters.Count - 1; waterIndex >= 0; --waterIndex)
                {
                    waters[waterIndex].Renderer.Render(cameraComponent, geometryType, commandBuffer, shader);
                }
            }
        }

        public void AddWaterMasksRenderCommands(CommandBuffer commandBuffer)
        {
            var waters = customWaterRenderList ?? WaterGlobals.Instance.Waters;

            for (int waterIndex = waters.Count - 1; waterIndex >= 0; --waterIndex)
                waters[waterIndex].Renderer.RenderMasks(commandBuffer);
        }

        private void RenderWaterEffects()
        {
            var waters = customWaterRenderList ?? WaterGlobals.Instance.Waters;

            for (int waterIndex = waters.Count - 1; waterIndex >= 0; --waterIndex)
                waters[waterIndex].Renderer.RenderEffects(this);
        }
        #region ImageEffectRenderModes

        internal void OnRenderImageCallback(RenderTexture source, RenderTexture destination)
        {
            // do not render in scene view if scene view renderin is disabled
            if (!RenderInSceneView && WaterUtilities.IsSceneViewCamera(cameraComponent))
            {
                Graphics.Blit(source, destination);
                return;
            }

            if (renderMode == WaterRenderMode.DefaultQueue)
            {
                Graphics.Blit(source, destination);
                return;
            }

            var waters = customWaterRenderList ?? WaterGlobals.Instance.Waters;
            for (int waterIndex = waters.Count - 1; waterIndex >= 0; --waterIndex)
            {
                waters[waterIndex].Volume.DisableRenderers();
            }

            switch (renderMode)
            {
                case WaterRenderMode.ImageEffectForward:
                {
                    _ForwardModule.Render(this, source, destination);
                    break;
                }

                case WaterRenderMode.ImageEffectDeferred:
                {
                    _DeferredModule.Render(this, source, destination);
                    break;
                }
            }
        }

        private void ReleaseImageEffectTemporaryTextures()
        {
            if (depthTex != null)
            {
                RenderTexture.ReleaseTemporary(depthTex);
                depthTex = null;
            }
        }

        private void AddUtilityCommands()
        {
            if (imageEffectCommands == null && renderMode == WaterRenderMode.ImageEffectDeferred)
            {
                imageEffectCommands = new CommandBuffer { name = "[PW Water] Set Buffers" };
                imageEffectCommands.SetGlobalTexture(ShaderUtility.Instance.PropertyId(ShaderVariableIDs.Gbuffer0), BuiltinRenderTextureType.GBuffer0);
                imageEffectCommands.SetGlobalTexture(ShaderUtility.Instance.PropertyId(ShaderVariableIDs.Gbuffer1), BuiltinRenderTextureType.GBuffer1);
                imageEffectCommands.SetGlobalTexture(ShaderUtility.Instance.PropertyId(ShaderVariableIDs.Gbuffer2), BuiltinRenderTextureType.GBuffer2);
                imageEffectCommands.SetGlobalTexture(ShaderUtility.Instance.PropertyId(ShaderVariableIDs.Gbuffer3), BuiltinRenderTextureType.Reflections);

#if UNITY_5_5_OR_NEWER
                imageEffectCommands.SetGlobalTexture(ShaderUtility.Instance.PropertyId(ShaderVariableIDs.WaterlessDepth), BuiltinRenderTextureType.ResolvedDepth);
#else
                imageEffectCommands.SetGlobalTexture(ShaderUtility.Instance.PropertyId(ShaderVariableIDs.WaterlessDepth), BuiltinRenderTextureType.Depth);
#endif

                cameraComponent.RemoveCommandBuffer(CameraEvent.AfterLighting, imageEffectCommands);
                cameraComponent.AddCommandBuffer(CameraEvent.AfterLighting, imageEffectCommands);
            }
        }

        private void RemoveUtilityCommands()
        {
            RemoveCommandBuffer(CameraEvent.AfterLighting, "[PW Water] Set Buffers");

            if (imageEffectCommands != null)
            {
                imageEffectCommands.Dispose();
                imageEffectCommands = null;
            }
        }

        /// <summary>
        /// Removes a command buffer by name. It's the most reliable way to remove a command buffer as references may be cleared in the editor.
        /// </summary>
        /// <param name="cameraEvent"></param>
        /// <param name="name"></param>
        private void RemoveCommandBuffer(CameraEvent cameraEvent, string name)
        {
            var buffers = cameraComponent.GetCommandBuffers(cameraEvent);

            for (int i = buffers.Length - 1; i >= 0; --i)
            {
                if (buffers[i].name == name)
                {
                    cameraComponent.RemoveCommandBuffer(cameraEvent, buffers[i]);
                    return;
                }
            }
        }
        #endregion ImageEffectRenderModes

        private void EnableEffects()
        {
            if (waterCameraType != CameraType.Normal)
                return;

            pixelWidth = Mathf.RoundToInt(cameraComponent.pixelWidth * baseEffectsQuality);
            pixelHeight = Mathf.RoundToInt(cameraComponent.pixelHeight * baseEffectsQuality);

            if (WaterProjectSettings.Instance.SinglePassStereoRendering)
            {
                pixelWidth = Mathf.CeilToInt((pixelWidth << 1) / 256.0f) * 256;
            }

            effectsEnabled = true;

            if (renderWaterDepth || renderVolumes)
                cameraComponent.depthTextureMode |= DepthTextureMode.Depth;

            _DepthModule.OnEnable(this);
        }

        private void DisableEffects()
        {
            effectsEnabled = false;
            _DepthModule.OnDisable(this);
        }

        private static bool IsWaterPossiblyVisible()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                return true;
#endif

            var waters = WaterGlobals.Instance.Waters;
            return waters.Count != 0;
        }

        private Camera CreateEffectsCamera(CameraType type)
        {
            var effectCameraGo = new GameObject(name + " Water Effects Camera") { hideFlags = HideFlags.HideAndDontSave };

            var effectCamera = effectCameraGo.AddComponent<Camera>();
            effectCamera.enabled = false;
            effectCamera.useOcclusionCulling = false;

            var effectWaterCamera = effectCameraGo.AddComponent<WaterCamera>();
            effectWaterCamera.waterCameraType = type;
            effectWaterCamera.mainCamera = cameraComponent;
            effectWaterCamera.baseCamera = this;

            enabledWaterCameras.Remove(effectWaterCamera);

            return effectCamera;
        }

        private void RenderShadowEnforcers()
        {
            if (shadowsEnforcerMesh == null)
            {
                shadowsEnforcerMesh = new Mesh
                {
                    name = "Water Shadow Enforcer",
                    hideFlags = HideFlags.DontSave,
                    vertices = new Vector3[4]
                };
                shadowsEnforcerMesh.SetIndices(new[] { 0, 1, 2, 3 }, MeshTopology.Quads, 0);
                shadowsEnforcerMesh.UploadMeshData(true);

                shadowsEnforcerMaterial = ShaderUtility.Instance.CreateMaterial(ShaderList.ShadowEnforcer, HideFlags.DontSave);
            }

            var bounds = new Bounds();

            float distance = QualitySettings.shadowDistance;
            var a = cameraComponent.ViewportPointToRay(new Vector3(shadowedWaterRect.xMin, shadowedWaterRect.yMin, 1.0f)).GetPoint(distance * 1.5f);
            var b = cameraComponent.ViewportPointToRay(new Vector3(shadowedWaterRect.xMax, shadowedWaterRect.yMax, 1.0f)).GetPoint(distance * 1.5f);
            SetBoundsMinMaxComponentWise(ref bounds, a, b);
            bounds.Encapsulate(cameraComponent.ViewportPointToRay(new Vector3(shadowedWaterRect.xMin, shadowedWaterRect.yMax, 1.0f)).GetPoint(distance * 0.3f));
            bounds.Encapsulate(cameraComponent.ViewportPointToRay(new Vector3(shadowedWaterRect.xMax, shadowedWaterRect.yMin, 1.0f)).GetPoint(distance * 0.3f));
            shadowsEnforcerMesh.bounds = bounds;

            Graphics.DrawMesh(shadowsEnforcerMesh, Matrix4x4.identity, shadowsEnforcerMaterial, 0);
        }

        private void PrepareToRender()
        {
            // reset shadowed water rect
            shadowedWaterRect = new Rect(1.0f, 1.0f, -1.0f, -1.0f);

            // find containing water
            float waterEnterTolerance = 1.0f + Mathf.Max(0.5f, cameraComponent.nearClipPlane) * Mathf.Tan(Mathf.Max(16.0f, cameraComponent.fieldOfView) * 0.5f * Mathf.Deg2Rad) * 3.0f;
            var newWater = Water.FindWater(transform.position, waterEnterTolerance, customWaterRenderList, out isInsideSubtractiveVolume, out isInsideAdditiveVolume);

            if (newWater != containingWater)
            {
                if (containingWater != null && submersionState != SubmersionState.None)
                {
                    submersionState = SubmersionState.None;
                    SubmersionStateChanged.Invoke(this);
                }

                containingWater = newWater;
                submersionState = SubmersionState.None;

                if (waterSample != null)
                {
                    waterSample.Stop();
                    waterSample = null;
                }

                if (newWater != null && newWater.Volume.Boundless)
                {
                    waterSample = new WaterSample(containingWater, WaterSample.DisplacementMode.Height, 0.4f);
                    waterSample.Start(transform.position);
                }
            }

            // determine submersion state
            SubmersionState newSubmersionState;

            if (waterSample != null)
            {
                waterLevel = waterSample.GetAndReset(transform.position).y;

                if (transform.position.y - waterEnterTolerance < waterLevel)
                    newSubmersionState = transform.position.y + waterEnterTolerance < waterLevel ? SubmersionState.Full : SubmersionState.Partial;
                else
                    newSubmersionState = SubmersionState.None;
            }
            else
            {
                newSubmersionState = containingWater != null ? SubmersionState.Partial : SubmersionState.None;          // for non-boundless water always use Partial state as determining this would be too costly
            }

            if (newSubmersionState != submersionState)
            {
                submersionState = newSubmersionState;
                SubmersionStateChanged.Invoke(this);
            }
        }

        private static void SetFallbackTextures()
        {
            Shader.SetGlobalTexture(ShaderUtility.Instance.PropertyId(ShaderVariableIDs.UnderwaterMask), TextureUtility.ClearTexture);
            Shader.SetGlobalTexture(ShaderUtility.Instance.PropertyId(ShaderVariableIDs.DisplacementsMask), TextureUtility.WhiteTexture);
        }

        private void ToggleEffects()
        {
            if (!effectsEnabled)
            {
                if (IsWaterPossiblyVisible())
                {
                    EnableEffects();
                }
            }
            else if (!IsWaterPossiblyVisible())
            {
                DisableEffects();
            }

            int width = Mathf.RoundToInt(cameraComponent.pixelWidth * baseEffectsQuality);
            int height = Mathf.RoundToInt(cameraComponent.pixelHeight * baseEffectsQuality);

            if (WaterProjectSettings.Instance.SinglePassStereoRendering)
            {
                width = Mathf.CeilToInt((width << 1) / 256.0f) * 256;
            }

            if (effectsEnabled && (width != pixelWidth || height != pixelHeight))
            {
                DisableEffects();
                EnableEffects();

                if (RenderTargetResized != null)
                {
                    RenderTargetResized(this);
                }
            }
        }

        private void SetPlaneProjectorMatrix(bool isRenderTarget, bool flipY)
        {
            var planeProjectorCamera = PlaneProjectorCamera;

            Shader.SetGlobalMatrix("_WaterProjectorPreviousVP", lastPlaneProjectorMatrix);

            planeProjectorCamera.CopyFrom(cameraComponent);
            planeProjectorCamera.renderingPath = RenderingPath.Forward;
            planeProjectorCamera.ResetProjectionMatrix();

            planeProjectorCamera.projectionMatrix = cameraComponent.projectionMatrix;

            var projection = flipY ? cameraComponent.projectionMatrix * Matrix4x4.Scale(new Vector3(1.0f, -1.0f, 1.0f)) : cameraComponent.projectionMatrix;
            lastPlaneProjectorMatrix = GL.GetGPUProjectionMatrix(projection, isRenderTarget) * cameraComponent.worldToCameraMatrix;

            Shader.SetGlobalMatrix("_WaterProjectorVP", lastPlaneProjectorMatrix);
        }

        private void SetLocalMapCoordinates()
        {
            var localMapsCoordsId = ShaderUtility.Instance.PropertyId(ShaderVariableIDs.LocalMapsCoords);
            var localMapsCoordsPreviousId = ShaderUtility.Instance.PropertyId(ShaderVariableIDs.LocalMapsCoordsPrevious);

            int resolution = Mathf.NextPowerOfTwo((cameraComponent.pixelWidth + cameraComponent.pixelHeight) >> 1);
            float maxHeight = 0.0f;
            float maxWaterLevel = 0.0f;

            var waters = WaterGlobals.Instance.Waters;

            for (int waterIndex = waters.Count - 1; waterIndex >= 0; --waterIndex)
            {
                var water = waters[waterIndex];
                maxHeight += water.MaxVerticalDisplacement;

                float posY = water.transform.position.y;
                if (maxWaterLevel < posY)
                    maxWaterLevel = posY;
            }

            // place camera
            Vector3 thisCameraPosition = cameraComponent.transform.position;
            Vector3 thisCameraForward = cameraComponent.transform.forward;
            float forwardFactor = Mathf.Min(1.0f, thisCameraForward.y + 1.0f);

            float size1 = Mathf.Abs(thisCameraPosition.y) * (1.0f + 7.0f * Mathf.Sqrt(forwardFactor));
            float size2 = maxHeight * 2.5f;
            float size = size1 > size2 ? size1 : size2;

            if (size < 20.0f)
                size = 20.0f;

            Vector3 effectCameraPosition = new Vector3(thisCameraPosition.x + thisCameraForward.x * size * 0.4f, 0.0f, thisCameraPosition.z + thisCameraForward.z * size * 0.4f);

            localMapsRectPrevious = localMapsRect;

            float halfPixelSize = size / resolution;
            localMapsRect = new Rect((effectCameraPosition.x - size) + halfPixelSize, (effectCameraPosition.z - size) + halfPixelSize, 2.0f * size, 2.0f * size);

            float invWidthPrevious = 1.0f / localMapsRectPrevious.width;
            Shader.SetGlobalVector(localMapsCoordsPreviousId, new Vector4(-localMapsRectPrevious.xMin * invWidthPrevious, -localMapsRectPrevious.yMin * invWidthPrevious, invWidthPrevious, localMapsRectPrevious.width));

            float invWidth = 1.0f / localMapsRect.width;
            Shader.SetGlobalVector(localMapsCoordsId, new Vector4(-localMapsRect.xMin * invWidth, -localMapsRect.yMin * invWidth, invWidth, localMapsRect.width));
        }
        #endregion Private Methods

        #region Helper Methods
        private static void SetBoundsMinMaxComponentWise(ref Bounds bounds, Vector3 a, Vector3 b)
        {
            if (a.x > b.x)
            {
                float t = b.x;
                b.x = a.x;
                a.x = t;
            }

            if (a.y > b.y)
            {
                float t = b.y;
                b.y = a.y;
                a.y = t;
            }

            if (a.z > b.z)
            {
                float t = b.z;
                b.z = a.z;
                a.z = t;
            }

            bounds.SetMinMax(a, b);
        }
        #endregion Helper Methods
    }
}