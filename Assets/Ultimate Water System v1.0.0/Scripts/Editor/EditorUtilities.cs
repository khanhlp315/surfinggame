﻿namespace UltimateWater.Editors
{
    using System.Collections.Generic;
    using System.Reflection;
    using UnityEditor;

    public static class Extentions
    {
        public static void AssignAllProperties(this Editor obj)
        {
            var fields = obj.GetType().GetFields(
              BindingFlags.FlattenHierarchy |
              BindingFlags.NonPublic |
              BindingFlags.Public |
              BindingFlags.Static | BindingFlags.Instance);

            foreach (var field in fields)
            {
                if (field.FieldType == typeof(SerializedProperty))
                {
                    var found = obj.serializedObject.FindProperty(field.Name);
                    if (found == null) continue;

                    field.SetValue(obj, found);
                }
            }
        }
        public static void AssignProperty(this Editor obj, string name)
        {
            var fields = obj.GetType().GetFields(
                BindingFlags.FlattenHierarchy |
                BindingFlags.NonPublic |
                BindingFlags.Public |
                BindingFlags.Static | BindingFlags.Instance);

            foreach (var field in fields)
            {
                if (field.Name == name)
                {
                    field.SetValue(obj, obj.serializedObject.FindProperty(name));
                }
            }
        }

        public static IEnumerable<SerializedProperty> GetChildren(this SerializedProperty property)
        {
            property = property.Copy();
            var nextElement = property.Copy();
            bool hasNextElement = nextElement.NextVisible(false);
            if (!hasNextElement)
            {
                nextElement = null;
            }

            property.NextVisible(true);
            while (true)
            {
                if ((SerializedProperty.EqualContents(property, nextElement)))
                {
                    yield break;
                }

                yield return property;

                bool hasNext = property.NextVisible(false);
                if (!hasNext)
                {
                    break;
                }
            }
        }
    }
}