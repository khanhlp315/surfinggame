﻿namespace UltimateWater.Editors
{
    using UnityEditor;

    [CustomEditor(typeof(WaterRipplesData))]
    public class WaterRipplesSettingsEditor : Editor
    {
        #region Unity Messages
        public void OnEnable()
        {
            this.AssignAllProperties();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var property = serializedObject.GetIterator();
            property.NextVisible(true);

            do
            {
                if (HandleLod(property))
                {
                    continue;
                }
                EditorGUILayout.PropertyField(property, true);
            } while (property.NextVisible(false));

            serializedObject.ApplyModifiedProperties();
        }
        #endregion Unity Messages

        #region Private Variables
#pragma warning disable 0649
        private SerializedProperty _Lod;
        private SerializedProperty _DisableLOD;
#pragma warning restore 0649
        #endregion Private Variables

        #region Private Methods
        private bool HandleLod(SerializedProperty property)
        {
            if (SerializedProperty.EqualContents(property, _Lod))
            {
                if (!_DisableLOD.boolValue)
                {
                    ListDrawer.Show(property, ListDrawer.EditorListOption.Default);
                }
                return true;
            }
            return false;
        }
        #endregion Private Methods
    }
}