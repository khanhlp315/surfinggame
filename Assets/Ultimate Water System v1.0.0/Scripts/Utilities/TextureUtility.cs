﻿namespace UltimateWater.Internal
{
    using UnityEngine;

    public static class TextureUtility
    {
        #region Public Types
        public class RenderTextureDesc
        {
            #region Public Variables
            public int Width;
            public int Height;
            public int Depth = 0;
            public int Antialiasing = 1;
            public string Name = "RT";
            public RenderTextureFormat Format = RenderTextureFormat.Default;
            public HideFlags Flags = HideFlags.DontSave;
            public FilterMode Filter = FilterMode.Bilinear;
            public TextureWrapMode Wrap = TextureWrapMode.Clamp;
            public bool ClearColor = true;
            public bool ClearDepth = true;
            public bool EnableRandomWrite = false;
            public RenderTextureReadWrite ColorSpace = RenderTextureReadWrite.Linear;
            #endregion Public Variables

            public RenderTextureDesc()
            {
            }

            public RenderTextureDesc(RenderTexture source)
            {
                Width = source.width;
                Height = source.height;
                Depth = source.depth;
                Antialiasing = source.antiAliasing;
                Name = source.name;
                Format = source.format;
                Flags = source.hideFlags;
                Filter = source.filterMode;
                Wrap = source.wrapMode;
                EnableRandomWrite = source.enableRandomWrite;
            }
        }
        #endregion Public Types

        #region Public Variables
        public static Texture2D ClearTexture
        {
            get
            {
                if (!_Initialzied)
                {
                    Initialize();
                }
                return _BlackTexture;
            }
        }
        public static Texture2D WhiteTexture
        {
            get
            {
                if (!_Initialzied)
                {
                    Initialize();
                }
                return _WhiteTexture;
            }
        }
        #endregion Public Variables

        #region Public Methods
        public static RenderTexture CreateTemporary(RenderTexture template)
        {
            var temporary = RenderTexture.GetTemporary(
                template.width, template.height,
                template.depth, template.format,
                template.sRGB ? RenderTextureReadWrite.sRGB : RenderTextureReadWrite.Linear,
                template.antiAliasing);

            if (temporary.wrapMode != template.wrapMode)
            {
                temporary.wrapMode = template.wrapMode;
            }

            if (temporary.filterMode != template.filterMode)
            {
                temporary.filterMode = template.filterMode;
            }

            return temporary;
        }

        public static RenderTexture CreateRenderTexture(RenderTextureDesc desc)
        {
            var result = new RenderTexture(desc.Width, desc.Height, desc.Depth, desc.Format, desc.ColorSpace)
            {
                hideFlags = desc.Flags,
                antiAliasing = desc.Antialiasing,
                filterMode = desc.Filter,
                wrapMode = desc.Wrap,
                name = desc.Name
            };

            if (desc.EnableRandomWrite)
            {
                if (result.IsCreated())
                {
                    result.Release();
                }
                result.enableRandomWrite = true;
                result.Create();
            }

            if (desc.ClearColor || desc.ClearDepth)
            {
                Clear(result, Color.clear, desc.ClearDepth && desc.Depth != 0, desc.ClearColor);
            }

            return result;
        }
        public static RenderTexture CreateRenderTexture(RenderTexture source)
        {
            var result = new RenderTexture(source.width, source.height, source.depth, source.format, RenderTextureReadWrite.Linear)
            {
                hideFlags = source.hideFlags,
                antiAliasing = source.antiAliasing,
                filterMode = source.filterMode,
                wrapMode = source.wrapMode,
            };

            if (source.enableRandomWrite)
            {
                if (result.IsCreated())
                {
                    result.Release();
                }
                result.enableRandomWrite = true;
            }

            result.Create();
            return result;
        }

        public static void Release(ref RenderTexture texture)
        {
            if (texture != null && texture.IsCreated())
            {
                texture.Release();
            }
            texture = null;
        }
        public static void ReleaseTemporary(ref RenderTexture texture)
        {
            if (texture != null)
            {
                RenderTexture.ReleaseTemporary(texture);
            }
            texture = null;
        }

        public static void Clear(RenderTexture texture, bool clearDepth = false, bool clearColor = true)
        {
            Clear(texture, Color.black, clearDepth, clearColor);
        }
        public static void Clear(RenderTexture texture, Color color, bool clearDepth = false, bool clearColor = true)
        {
            Graphics.SetRenderTarget(texture);
            GL.Clear(clearDepth, clearColor, color);
            Graphics.SetRenderTarget(null);
        }

        public static void Swap<T>(ref T left, ref T right)
        {
            var temp = left;
            left = right;
            right = temp;
        }

        // Resizes valid(not-null, and created) RenderTexture
        public static void Resize(RenderTexture texture, int width, int height)
        {
            if (texture == null)
            {
                Debug.LogWarning("Trying to resize null RenderTexture");
                return;
            }
            if (!texture.IsCreated())
            {
                Debug.LogWarning("Trying to resize not created RenderTexture");
                return;
            }

            if (width <= 0 || height <= 0)
            {
                Debug.LogWarning("Trying to resize to invalid(<=0) width or height ");
                return;
            }

            if (texture.width == width && texture.height == height)
            {
                return;
            }

            texture.Release();
            texture.width = width;
            texture.height = height;
            texture.Create();
        }

        // Checks if RenderTexture was "lost" and recreates it if needed
        // return: did the texture was recreated?
        public static bool Verify(RenderTexture texture, bool clear = true)
        {
            if (texture == null)
            {
                Debug.LogWarning("Trying to resolve null RenderTexture");
                return false;
            }

            if (texture.IsCreated())
            {
                return false;
            }
            texture.Create();

            if (clear)
            {
                Clear(texture, Color.clear);
            }
            return true;
        }
        #endregion Public Methods

        #region Private Variables
        private static Texture2D _BlackTexture;
        private static Texture2D _WhiteTexture;
        private static bool _Initialzied;
        #endregion Private Variables

        #region Private Methods
        private static void Initialize()
        {
            _BlackTexture = CreateColorTexure(Color.clear, "texture [default-black]");
            _WhiteTexture = CreateColorTexure(Color.white, "texture [default-white]");

            _Initialzied = true;
        }

        private static Texture2D CreateColorTexure(Color color, string name)
        {
            var result = new Texture2D(2, 2, TextureFormat.ARGB32, false)
            {
                name = name,
                hideFlags = HideFlags.DontSave
            };

            result.SetPixel(0, 0, color);
            result.SetPixel(1, 0, color);
            result.SetPixel(0, 1, color);
            result.SetPixel(1, 1, color);

            result.Apply(false, true);

            return result;
        }
        #endregion Private Methods
    }
}