﻿using UnityEngine;

namespace UltimateWater
{
    /// <summary>
    /// Small utility class used to display Unity's builtin gradient inspector.
    /// </summary>
    public class GradientContainer : ScriptableObject
    {
        public Gradient gradient;
    }
}