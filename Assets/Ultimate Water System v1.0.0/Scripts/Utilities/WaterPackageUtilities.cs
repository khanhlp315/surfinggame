﻿using System.IO;

namespace UltimateWater
{
    /// <summary>
    /// Helps locating the UltimateWater folder and find stuff in it.
    /// </summary>
    public class WaterPackageUtilities
    {
#if UNITY_EDITOR
        private static readonly string waterSpecificPath = "Ultimate Water System" + Path.DirectorySeparatorChar + "Graphics";
        private static string waterPackagePath;

        public static string WaterPackagePath
        {
            get
            {
                return waterPackagePath ?? (waterPackagePath = Find("Assets" + Path.DirectorySeparatorChar, ""));
            }
        }

#if UNITY_EDITOR
        public static T FindDefaultAsset<T>(string searchString, string searchStringFallback) where T : UnityEngine.Object
        {
            var guids = UnityEditor.AssetDatabase.FindAssets(searchString);

            if (guids.Length == 0)
                guids = UnityEditor.AssetDatabase.FindAssets(searchStringFallback);

            if (guids.Length == 0)
                return null;

            UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
            string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[0]);
            return UnityEditor.AssetDatabase.LoadAssetAtPath<T>(path);
        }
#endif

        private static string Find(string directory, string parentDirectory)
        {
            if (directory.EndsWith(waterSpecificPath))
                return parentDirectory.Replace(Path.DirectorySeparatorChar, '/');

            foreach (string subDirectory in Directory.GetDirectories(directory))
            {
                string result = Find(subDirectory, directory);

                if (result != null)
                    return result;
            }

            return null;
        }
#endif
    }
}