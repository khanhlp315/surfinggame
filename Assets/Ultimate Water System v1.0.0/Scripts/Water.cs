﻿namespace UltimateWater
{
#if UNITY_EDITOR

    using UnityEditor;

#endif

    using System;
    using System.Collections.Generic;

    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.Serialization;

    using Internal;

    /// <summary>
    ///     Main water component.
    /// </summary>
    [ExecuteInEditMode]
    [AddComponentMenu("Ultimate Water/Water (Base Component)", -1)]
    public sealed class Water : MonoBehaviour, ISerializationCallbackReceiver
    {
        #region Public Types
        [System.Serializable]
        public struct WeightedProfile
        {
            public WaterProfileData Profile;
            public float Weight;

            public WeightedProfile(WaterProfile profile, float weight)
            {
                var forceInitialize = profile.Data.Spectrum;
                if (forceInitialize == null)
                {
                    Debug.Log("spectrum not created");
                }

                Profile = new WaterProfileData();
                Profile.Copy(profile.Data);
                Profile.TemplateProfile = profile;

                Weight = weight;
            }
        }

        [Serializable]
        public class WaterEvent : UnityEvent<Water> { };
        #endregion Public Types

        #region Public Variables
        [Tooltip("Synchronizes dynamic preset with default water profile")]
        public bool Synchronize = true;

        public LayerMask CustomEffectsLayerMask
        {
            get { return dynamicWaterData.CustomEffectsLayerMask; }
            set { dynamicWaterData.CustomEffectsLayerMask = value; }
        }

        public WaterMaterials Materials
        {
            get { return materials; }
        }
        public ProfilesManager ProfilesManager
        {
            get { return profilesManager; }
        }
        public WaterGeometry Geometry
        {
            get { return geometry; }
        }
        public WaterRenderer Renderer
        {
            get { return waterRenderer; }
        }
        public WaterUvAnimator UVAnimator
        {
            get { return uvAnimator; }
        }
        public WaterVolume Volume
        {
            get { return volume; }
            set { volume = value; }
        }
        public DynamicWater DynamicWater
        {
            get { return dynamicWater; }
        }
        public Foam Foam
        {
            get { return foam; }
        }
        public PlanarReflection PlanarReflection
        {
            get { return planarReflection; }
        }
        public WaterSubsurfaceScattering SubsurfaceScattering
        {
            get { return subsurfaceScattering; }
        }
        public WindWaves WindWaves
        {
            get { return windWaves; }
        }

        public ShaderSet ShaderSet
        {
            get
            {
#if UNITY_EDITOR
                if (shaderSet == null)
                    shaderSet = WaterPackageUtilities.FindDefaultAsset<ShaderSet>("\"Ocean\" t:ShaderSet", "t:ShaderSet");
#endif

                return shaderSet;
            }
        }

        /// <summary>
        ///		Use this property instead of disabling water completely, if you want to either:
        ///			- temporarily disable water from rendering without releasing its resources so that enabling it again won't cause hiccups,
        ///			- disable water from rendering, but keep the physics.
        /// </summary>
        public bool RenderingEnabled
        {
            get { return renderingEnabled; }
            set
            {
                if (renderingEnabled == value)
                    return;

                renderingEnabled = value;

                if (renderingEnabled)
                {
                    if (enabled)
                        WaterGlobals.Instance.AddWater(this);
                }
                else
                    WaterGlobals.Instance.RemoveWater(this);
            }
        }

        public int ComputedSamplesCount
        {
            get { return activeSamplesCount; }
        }
        public float Density
        {
            get { return density; }
        }
        public float Gravity
        {
            get { return gravity; }
        }
        public float MaxHorizontalDisplacement
        {
            get { return maxHorizontalDisplacement; }
        }
        public float MaxVerticalDisplacement
        {
            get { return maxVerticalDisplacement; }
        }
        public int Seed
        {
            get { return seed; }
            set { seed = value; }
        }
        public Vector2 SurfaceOffset
        {
            get { return float.IsNaN(surfaceOffset.x) ? new Vector2(-transform.position.x, -transform.position.z) : surfaceOffset; }
            set { surfaceOffset = value; }
        }
        public float Time
        {
            get { return time == -1 ? UnityEngine.Time.time : time; }
            set { time = value; }
        }
        public float UniformWaterScale
        {
            get { return transform.localScale.y; }
        }

        public int WaterId
        {
            get { return waterId; }
        }

        public bool AskForWaterCamera = true;
        #endregion Public Variables

        #region Inspector Variables
        [SerializeField]
        [FormerlySerializedAs("shaderCollection")]
        private ShaderSet shaderSet;

        [Tooltip("Set it to anything else than 0 if your game has multiplayer functionality or you want your water to behave the same way each time your game is played (good for intro etc.).")]
        [SerializeField]
        private int seed;

        [SerializeField] private WaterMaterials materials;
        [SerializeField] private ProfilesManager profilesManager;
        [SerializeField] private WaterGeometry geometry;
        [SerializeField] private WaterRenderer waterRenderer;
        [SerializeField] private WaterUvAnimator uvAnimator;
        [SerializeField] private WaterVolume volume;
        [SerializeField] private WaterSubsurfaceScattering subsurfaceScattering;

        [SerializeField] private DynamicWater.Data dynamicWaterData;
        [SerializeField] private Foam.Data foamData;
        [SerializeField] private PlanarReflection.Data planarReflectionData;
        [SerializeField] private WindWaves.Data windWavesData;

        [SerializeField] private bool dontRotateUpwards;
        [SerializeField] private bool fastEnableDisable;

        [SerializeField]
#pragma warning disable 0414
        private float version = 0.4f;
#pragma warning restore 0414
        #endregion Inspector Variables

        #region Private Variables
        private readonly List<IWaterModule> _Modules = new List<IWaterModule>();

        private DynamicWater dynamicWater;
        private Foam foam;
        private PlanarReflection planarReflection;
        private WindWaves windWaves;

        private bool componentsCreated;
        private int waterId = -1;
        private int activeSamplesCount;
        private Vector2 surfaceOffset = new Vector2(float.NaN, float.NaN);
        private float maxHorizontalDisplacement;
        private float maxVerticalDisplacement;
        private float time = -1.0f;
        private float density;
        private float gravity;
        private bool renderingEnabled = true;

        public event Action WaterIdChanged;

        private static bool isPlaying;
        private static int nextWaterId = 256;
        private static readonly bool[] idUsageRegister = new bool[256];

        private static readonly Collider[] collidersBuffer = new Collider[30];
        private static readonly List<Water> possibleWaters = new List<Water>();
        private static readonly List<Water> excludedWaters = new List<Water>();

        #endregion Private Variables

        #region Unity Messages
        private void Awake()
        {
            WaterQualitySettings.Instance.Changed -= OnQualitySettingsChanged;
            WaterQualitySettings.Instance.Changed += OnQualitySettingsChanged;
        }

        private void OnEnable()
        {
            if (!Application.isPlaying) return;

            if (fastEnableDisable && componentsCreated)
                return;

            isPlaying = Application.isPlaying;          // can't access it from OnAfterDeserialize in other way

            CreateWaterComponents();
            AssignId();

            if (!componentsCreated)
            {
                return;
            }

            _Modules.ForEach(x => x.Enable());

            if (renderingEnabled)
            {
                WaterGlobals.Instance.AddWater(this);
            }
        }

        private void OnDisable()
        {
            if (!Application.isPlaying) return;

            if (fastEnableDisable)
                return;

            _Modules.ForEach(x => x.Disable());

            FreeId();
            WaterGlobals.Instance.RemoveWater(this);
        }

        private void OnDestroy()
        {
            if (fastEnableDisable)
            {
                fastEnableDisable = false;
                OnDisable();
            }

            WaterQualitySettings.Instance.Changed -= OnQualitySettingsChanged;

            _Modules.ForEach(x => x.Destroy());
            _Modules.Clear();
        }

        private void Update()
        {
            if (!Application.isPlaying) return;

            if (!dontRotateUpwards)
                transform.eulerAngles = new Vector3(0.0f, transform.eulerAngles.y, 0.0f);

            UpdateStatisticalData();

            _Modules.ForEach(x => x.Update());
        }

        public void OnValidate()
        {
            if (!componentsCreated && isActiveAndEnabled)
            {
                bool enableComponents = enabled && Application.isPlaying;

                if (enableComponents)
                    OnDisable();

                CreateWaterComponents();

                if (enableComponents)
                    OnEnable();
            }

            if (componentsCreated)
            {
                _Modules.ForEach(x => x.Validate());
            }
        }
        public void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan * 0.2f;
            if (geometry != null && geometry.GeometryType == WaterGeometry.Type.CustomMeshes)
            {
                var meshes = geometry.CustomSurfaceMeshes;
                for (int i = 0; i < meshes.Meshes.Length; ++i)
                {
                    Gizmos.color = Color.cyan * 0.2f;
                    Gizmos.DrawMesh(meshes.Meshes[i]);

                    Gizmos.color = Color.cyan * 0.4f;
                    Gizmos.DrawWireMesh(meshes.Meshes[i]);
                }
            }
        }
        #endregion Unity Messages

        #region Public Methods
        /// <summary>
        /// Use this in your custom script that tries to access water very early after a scene gets loaded.
        /// </summary>
        public void ForceStartup()
        {
            CreateWaterComponents();
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            if (!isPlaying)
                componentsCreated = false;
        }

        public void ResetWater()
        {
            enabled = false;
            OnDestroy();
            componentsCreated = false;
            enabled = true;
        }
        #region Elevation sampling

        /// <summary>
        /// Computes water displacement vector at a given coordinates. WaterSample class does the same thing asynchronously and is recommended.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        /// <param name="spectrumEnd"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public Vector3 GetDisplacementAt(float x, float z, float time)
        {
            var result = new Vector3();
            if (windWaves != null)
            {
                result = windWaves.GetDisplacementAt(x, z, time);
            }

            return result;
        }

        /// <summary>
        /// Computes horizontal displacement vector at a given coordinates. WaterSample class does the same thing asynchronously and is recommended.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public Vector2 GetHorizontalDisplacementAt(float x, float z, float time)
        {
            var result = new Vector2();
            if (windWaves != null)
            {
                result = windWaves.GetHorizontalDisplacementAt(x, z, time);
            }

            return result;
        }

        /// <summary>
        /// Computes height at a given coordinates. WaterSample class does the same thing asynchronously and is recommended.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public float GetHeightAt(float x, float z, float time)
        {
            var result = 0.0f;
            if (windWaves != null)
            {
                result = windWaves.GetHeightAt(x, z, time);
            }

            return result;
        }

        /// <summary>
        /// Computes forces and height at a given coordinates. WaterSample class does the same thing asynchronously and is recommended.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="z"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public Vector4 GetHeightAndForcesAt(float x, float z, float time)
        {
            var result = Vector4.zero;
            if (windWaves != null)
            {
                result = windWaves.GetForceAndHeightAt(x, z, time);
            }

            return result;
        }

        #endregion Elevation sampling

        public static Water CreateWater(string name, ShaderSet shaderCollection)
        {
            var gameObject = new GameObject(name);
            var water = gameObject.AddComponent<Water>();
            water.shaderSet = shaderCollection;
            return water;
        }

        public static Water FindWater(Vector3 position, float radius)
        {
            bool unused1, unused2;
            return FindWater(position, radius, null, out unused1, out unused2);
        }

        public static Water FindWater(Vector3 position, float radius, out bool isInsideSubtractiveVolume, out bool isInsideAdditiveVolume)
        {
            return FindWater(position, radius, null, out isInsideSubtractiveVolume, out isInsideAdditiveVolume);
        }

        public static Water FindWater(Vector3 position, float radius, List<Water> allowedWaters, out bool isInsideSubtractiveVolume, out bool isInsideAdditiveVolume)
        {
            isInsideSubtractiveVolume = false;
            isInsideAdditiveVolume = false;

#if UNITY_5_2 || UNITY_5_1 || UNITY_5_0
            var collidersBuffer = Physics.OverlapSphere(position, radius, 1 << WaterProjectSettings.Instance.WaterCollidersLayer, QueryTriggerInteraction.Collide);
            int numHits = collidersBuffer.Length;
#else
            int numHits = Physics.OverlapSphereNonAlloc(position, radius, collidersBuffer, 1 << WaterProjectSettings.Instance.WaterCollidersLayer, QueryTriggerInteraction.Collide);
#endif

            possibleWaters.Clear();
            excludedWaters.Clear();

            for (int i = 0; i < numHits; ++i)
            {
                var volume = WaterVolumeBase.GetWaterVolume(collidersBuffer[i]);

                if (volume != null)
                {
                    if (volume is WaterVolumeAdd)
                    {
                        isInsideAdditiveVolume = true;

                        if (allowedWaters == null || allowedWaters.Contains(volume.Water))
                            possibleWaters.Add(volume.Water);
                    }
                    else                // subtractive
                    {
                        isInsideSubtractiveVolume = true;
                        excludedWaters.Add(volume.Water);
                    }
                }
            }

            for (int i = 0; i < possibleWaters.Count; ++i)
            {
                if (!excludedWaters.Contains(possibleWaters[i]))
                    return possibleWaters[i];
            }

            var boundlessWaters = WaterGlobals.Instance.BoundlessWaters;
            int numBoundlessWaters = boundlessWaters.Count;

            for (int i = 0; i < numBoundlessWaters; ++i)
            {
                var water = boundlessWaters[i];

                if ((allowedWaters == null || allowedWaters.Contains(water)) && water.Volume.IsPointInsideMainVolume(position, radius) && !excludedWaters.Contains(water))
                    return water;
            }

            return null;
        }
        #endregion Public Methods

        #region Private Methods
        internal void OnWaterRender(WaterCamera waterCamera)
        {
            if (!isActiveAndEnabled) return;

            for (int i = 0; i < _Modules.Count; ++i)
            {
                _Modules[i].OnWaterRender(waterCamera);
            }
        }

        internal void OnWaterPostRender(WaterCamera waterCamera)
        {
            for (int i = 0; i < _Modules.Count; ++i)
            {
                _Modules[i].OnWaterPostRender(waterCamera);
            }
        }
        internal void OnSamplingStarted()
        {
            ++activeSamplesCount;
        }

        internal void OnSamplingStopped()
        {
            --activeSamplesCount;
        }

        /// <summary>
        /// Creates some internal management classes, depending if they are needed by the used shader collection.
        /// </summary>
        private void CreateWaterComponents()
        {
            if (componentsCreated)
                return;

            componentsCreated = true;

            _Modules.Clear();
            _Modules.AddRange(new List<IWaterModule> {
                materials,
                geometry,
                profilesManager,
                waterRenderer,
                uvAnimator,
                volume,
                subsurfaceScattering
            });

            for (int i = 0; i < _Modules.Count; ++i)
            {
                _Modules[i].Start(this);
            }

            profilesManager.Changed.AddListener(OnProfilesChanged);

            if (shaderSet.LocalEffectsSupported)
            {
                dynamicWater = new DynamicWater(this, dynamicWaterData);
                _Modules.Add(dynamicWater);
            }

            if (shaderSet.PlanarReflections != PlanarReflectionsMode.Disabled)
            {
                planarReflection = new PlanarReflection(this, planarReflectionData);
                _Modules.Add(planarReflection);
            }

            if (shaderSet.WindWavesMode != WindWavesRenderMode.Disabled)
            {
                windWaves = new WindWaves(this, windWavesData);
                _Modules.Add(windWaves);
            }

            if (shaderSet.Foam)
            {
                foam = new Foam(this, foamData);   // has to be after wind waves
                _Modules.Add(foam);
            }
        }

        internal void OnProfilesChanged(Water water)
        {
            var profiles = water.ProfilesManager.Profiles;

            density = 0.0f;
            gravity = 0.0f;

            for (int i = 0; i < profiles.Length; ++i)
            {
                var profile = profiles[i].Profile;
                float weight = profiles[i].Weight;

                density += profile.Density * weight;
                gravity -= profile.Gravity * weight;
            }
        }

        private void OnQualitySettingsChanged()
        {
            OnValidate();
        }

        private void UpdateStatisticalData()
        {
            maxHorizontalDisplacement = 0.0f;
            maxVerticalDisplacement = 0.0f;

            if (windWaves != null)
            {
                maxHorizontalDisplacement = windWaves.MaxHorizontalDisplacement;
                maxVerticalDisplacement = windWaves.MaxVerticalDisplacement;
            }
        }

        private void AssignId()
        {
            if (waterId != -1)
                return;         // already assigned

            for (waterId = 1; waterId < 256; ++waterId)
            {
                if (!idUsageRegister[waterId])
                {
                    idUsageRegister[waterId] = true;
                    break;
                }
            }

            if (waterId == 256)
                waterId = nextWaterId++;

            if (WaterIdChanged != null)
                WaterIdChanged();
        }

        private void FreeId()
        {
            if (waterId == -1)
                return;             // already freed

            if (waterId < idUsageRegister.Length)
                idUsageRegister[waterId] = false;

            waterId = -1;
        }
        #endregion Private Methods

#if UNITY_EDITOR
        [MenuItem("GameObject/Water/Water (Base Component)", false, -1)]
        private static void CreateCustomGameObject(MenuCommand menuCommand)
        {
            var obj = new GameObject("Water");
            obj.AddComponent<Water>();

            GameObjectUtility.SetParentAndAlign(obj, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(obj, "Create " + obj.name);
            Selection.activeObject = obj;
        }
#endif
    }
}