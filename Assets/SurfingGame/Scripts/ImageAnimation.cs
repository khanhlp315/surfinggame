﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageAnimation : MonoBehaviour
{

	[SerializeField] private Sprite[] _sprites;

	private Image _image;

	[SerializeField]
	private float _delay;

	[SerializeField]
	private GameObject _fireworks;

	void Awake()
	{
		_image = GetComponent<Image>();
	}
	
	public void RunAnimation()
	{
		StopAllCoroutines();
		StartCoroutine(StartAnimation());
	}

	private IEnumerator StartAnimation()
	{
		foreach (var sprite in _sprites)
		{
			_image.sprite = sprite;
			yield return new WaitForSeconds(_delay);
		}
		FindObjectOfType<PlayerController>().IsPlaying = false;
		_fireworks.SetActive(true);
		FindObjectOfType<ImageAnimation>().StopAllCoroutines();


		yield return null;
	}
}
