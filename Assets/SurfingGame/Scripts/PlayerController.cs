﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	[SerializeField] private Rigidbody _rigidbody;
	[SerializeField] private Text _score;
	[SerializeField] private Transform _boatMesh;
	private int point;
	
	[HideInInspector] public bool inputKeyW = false;
	[HideInInspector] public bool inputKeyS = false;
	[HideInInspector] public bool inputKeyA = false;
	[HideInInspector] public bool inputKeyD = false;

	[SerializeField] private Text _winText;
	
	public Vector3 rotationLimits = new Vector3(0.0f,0.0f,-30.0f);


	private float moveForward;
	private float moveSideways;
	private float xMove;
	private float zMove;
	public float speed = 0.1f;
	public float rotateSpeed = 0.5f;

	private bool isPlaying = true;

    private SurfingGestureListener gestureListener;
    [HideInInspector] public bool inputKeyLeanForward = false;
    [HideInInspector] public bool inputKeyLeanLeft = false;
    [HideInInspector] public bool inputKeyLeanRight = false;

	private float tempSpeed;

    private void Start()
    {
	    tempSpeed = speed;
        gestureListener = SurfingGestureListener.Instance;
    }

    public bool IsPlaying
	{
		get { return isPlaying; }
		set
		{
			isPlaying = value;
			if (!isPlaying)
			{
				_winText.transform.parent.gameObject.SetActive(true);
				_winText.text = point.ToString();
			}
		}
	}

	private void OnCollisionEnter(Collision other)
	{
		Debug.Log(other.gameObject.tag);
		if (other.gameObject.CompareTag("Present"))
		{
			point++;
			other.gameObject.SetActive(false);
			_score.text = point.ToString();
		}
	}

	private void Update()
	{
		if (!isPlaying)
		{
			inputKeyW = false;
			inputKeyA = false;
			inputKeyD = false;
			inputKeyS = false;
			inputKeyLeanForward = false;
			inputKeyLeanLeft = false;
			inputKeyLeanRight = false;
			return;
		}
		
		//inputKeyW = Input.GetKey("w");
		//inputKeyS = Input.GetKey("s");
		//inputKeyA = Input.GetKey("a");
		//inputKeyD = Input.GetKey("d");

        if (gestureListener)
        {
            inputKeyLeanForward = gestureListener.IsLeanLeft() || gestureListener.IsLeanRight();
            inputKeyLeanLeft = gestureListener.IsLeanLeft();
            inputKeyLeanRight = gestureListener.IsLeanRight();
        }

        inputKeyW = Input.GetKey("w") || inputKeyLeanForward;
        inputKeyS = Input.GetKey("s");
        inputKeyA = Input.GetKey("a") || inputKeyLeanLeft;
        inputKeyD = Input.GetKey("d") || inputKeyLeanRight;

        if (inputKeyA)
		{
			var eulerAngles = transform.eulerAngles;
			eulerAngles.y -= rotateSpeed * Time.deltaTime;
			transform.eulerAngles = eulerAngles;
		}
		else if (inputKeyD)
		{
			var eulerAngles = transform.eulerAngles;
			eulerAngles.y += rotateSpeed * Time.deltaTime;
			transform.eulerAngles = eulerAngles;
		}
	}

	private void FixedUpdate()
	{
		if (!isPlaying)
		{
			inputKeyW = false;
			inputKeyA = false;
			inputKeyD = false;
			inputKeyS = false;
			inputKeyLeanForward = false;
			inputKeyLeanLeft = false;
			inputKeyLeanRight = false;
			return;
		}
		if (inputKeyW)
		{
			if (inputKeyA || inputKeyD)
			{
				tempSpeed = speed;
				var moveVector = transform.forward * speed;
				moveVector.y = 0;
				Debug.Log(moveVector);
				_rigidbody.MovePosition(_rigidbody.position + moveVector);
			}
			else if(tempSpeed > 0)
			{
				tempSpeed -= speed * Time.deltaTime / 3.0f;
				if (tempSpeed < 0)
				{
					tempSpeed = 0;
				}
				var moveVector = transform.forward * speed;
				moveVector.y = 0;
				Debug.Log(moveVector);
				_rigidbody.MovePosition(_rigidbody.position + moveVector);
			}

		}



	}
	
	private void LateUpdate() {

		//clamp rotations
		if (transform.eulerAngles.x < 330)
		{
			var eulerAngles = transform.eulerAngles;
			eulerAngles.x = 330;
			transform.eulerAngles = eulerAngles;
		}
		if (transform.eulerAngles.x > 30)
		{
			var eulerAngles = transform.eulerAngles;
			eulerAngles.x = 30;
			transform.eulerAngles = eulerAngles;
		}
		
		if (transform.eulerAngles.z < 340)
		{
			var eulerAngles = transform.eulerAngles;
			eulerAngles.z = 340;
			transform.eulerAngles = eulerAngles;
		}
		if (transform.eulerAngles.z > 20)
		{
			var eulerAngles = transform.eulerAngles;
			eulerAngles.z = 20;
			transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
		}
	}

	public void ResetGame()
	{
		isPlaying = true;
		point = 0;
		_score.text = point.ToString();
		_winText.transform.parent.gameObject.SetActive(false);

	}
}
