﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{

	[SerializeField]
	private GameObject _videoObject;

	[SerializeField] 
	private GameObject _sceneObject;

	private PresentController[] _presents;

	private Transform _player;

	private Vector3[] _presentPosition;

	private Vector3 _playerPosition;
	private Quaternion _playerRotation;

	private TargetController _target;

	private ImageAnimation _imageAnimation;
	
	void Start()
	{
		_presents = FindObjectsOfType<PresentController>();
		_player = FindObjectOfType<PlayerController>().transform;
		_presentPosition = _presents.Select(x => x.transform.position).ToArray();
		_playerPosition = _player.transform.position;
		_playerRotation = _player.transform.rotation;
		_target = FindObjectOfType<TargetController>();
		_imageAnimation = FindObjectOfType<ImageAnimation>();
		_sceneObject.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Return))
		{
			_sceneObject.SetActive(true);
			_videoObject.SetActive(false);
			_imageAnimation.RunAnimation();
		}
		
		else if (Input.GetKeyDown(KeyCode.Escape))
		{
			ResetGame();
			_sceneObject.SetActive(false);
			_videoObject.SetActive(true);
		}
	}

	void ResetGame()
	{
		for (int i = 0; i < _presents.Length; ++i)
		{
			var present = _presents[i];
			present.gameObject.SetActive(true);
			present.transform.position = _presentPosition[i];
		}
		_player.transform.position = _playerPosition;
		_player.transform.rotation = _playerRotation;
		_player.GetComponent<PlayerController>().ResetGame();
		_target.ResetGame();

	}
}
