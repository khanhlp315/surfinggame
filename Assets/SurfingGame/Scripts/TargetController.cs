﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    [SerializeField]
    private GameObject _fireworks;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Win");
            other.gameObject.GetComponent<PlayerController>().IsPlaying = false;
            _fireworks.SetActive(true);
            FindObjectOfType<ImageAnimation>().StopAllCoroutines();
        }
    }

    public void ResetGame()
    {
        _fireworks.SetActive(false);
    }
}
